name := "Vegas_Trainer"


version := "2.0"


scalaVersion := "2.12.4"
//mainClass in (Compile, run) := Some("algoTrading.vegas.kernel.VegasOptimizer")
//mainClass in (Compile, packageBin) := Some("algoTrading.vegas.kernel.VegasOptimizer")

libraryDependencies += "org.apache.commons" % "commons-math3" % "3.6.1"
libraryDependencies += "org.apache.commons" % "commons-csv" % "1.5"
libraryDependencies += "org.apache.poi" % "poi" % "4.0.1"
libraryDependencies += "org.apache.poi" % "poi-ooxml" % "4.0.1"
libraryDependencies += "org.deeplearning4j" % "deeplearning4j-core" %  "1.0.0-beta3"
libraryDependencies += "org.nd4j" % "nd4j-native-platform" %  "1.0.0-beta3"
libraryDependencies += "org.datavec" % "datavec-api" %  "1.0.0-beta3"
