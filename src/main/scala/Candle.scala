package algoTrading.velas.kernel

import java.nio.ByteBuffer

import scala.collection.LinearSeq

object Candle {
	lazy val openTimeIndex = 0
	lazy val openIndex = java.lang.Long.BYTES
	lazy val highIndex = java.lang.Long.BYTES + java.lang.Float.BYTES
	lazy val lowIndex = java.lang.Long.BYTES + 2 * java.lang.Float.BYTES
	lazy val closeIndex = java.lang.Long.BYTES + 3 * java.lang.Float.BYTES
	lazy val pointerIndex = java.lang.Long.BYTES + 4 * java.lang.Float.BYTES
	lazy val ticksIndex = 2 * java.lang.Long.BYTES + 4 * java.lang.Float.BYTES
	lazy val maxTimeIndex = 2 * java.lang.Long.BYTES + 4 * java.lang.Float.BYTES + Integer.BYTES
	lazy val minTimeIndex = 3 * java.lang.Long.BYTES + 4 * java.lang.Float.BYTES + Integer.BYTES
	lazy val byteLength = 4 * java.lang.Long.BYTES + 4 * java.lang.Float.BYTES + Integer.BYTES
	

	
	private def getTimeCandleKms(k: Int, x: Long) = x - (x % k)
	
	def TiempoVelaMasReciente(tActual: Long, candleLength: Int, unit: String) = {
		val k = units_to_long(unit).toInt
		getTimeCandleKms(candleLength * k, tActual)
	}
	
	private def units_to_long(unit: String):Long =
	{
		if (unit.toLowerCase.startsWith("sec")) 1000L
		else if (unit.toLowerCase.startsWith("min")) 1000L * 60L
		else if (unit.toLowerCase.startsWith("hou")) 1000L * 60L * 60L
		else if (unit.toLowerCase.startsWith("day")) 1000L * 60L * 60L * 24L
		else if (unit.toLowerCase.startsWith("wee")) 1000L * 60L * 60L * 24L * 7L
		else if (unit.toLowerCase.startsWith("mon")) 1000L * 60L * 60L * 24L * 30L
		else 1L
	}
	def apply(openTime: Long, closeTime: Long, ticks: LinearSeq[Tick], ticks_before:Long):Candle =
	{
		val firstTick:Tick=ticks.head
		val init:(Tick,Tick,Tick,Tick,Int)=(firstTick,firstTick,firstTick,firstTick,1)
		def reducer(ohlc:(Tick,Tick,Tick,Tick,Int),ticks: LinearSeq[Tick]):(Tick,Tick,Tick,Tick,Int)
		=
		{
			
			val actualTick:Tick=ticks.head
			
			if(actualTick.dateTime<= closeTime)
				{
					val midP:Float=(actualTick.bid+actualTick.ask)/2;
					val high:Float=(ohlc._2.bid+ohlc._2.ask)/2
					val low:Float=(ohlc._3.bid+ohlc._3.ask)/2
					reducer((ohlc._1,if(midP>high) actualTick else ohlc._2,if(midP<low) actualTick else ohlc._3,actualTick,ohlc._5+1),ticks.tail)
				}
			else ohlc
			
		}
		
		val result=reducer(init,ticks.tail)
		
	new Candle(
		(result._1.bid+result._1.ask)/2,
		(result._2.bid+result._2.ask)/2,
		(result._3.bid+result._3.ask)/2,
		(result._4.bid+result._4.ask)/2,
		openTime,
		closeTime,
		result._2.dateTime,
		result._3.dateTime,
		result._5,
		ticks_before
	)
	}
	
	def apply(bytes: Array[Byte], length: Long):Candle=
	{
		val buffer: ByteBuffer = ByteBuffer.wrap (bytes)
		val open_Time = buffer.getLong (openTimeIndex)
		val close_Time = open_Time + length
		
	new Candle(
		buffer.getFloat (openIndex),
		buffer.getFloat (highIndex),
		buffer.getFloat (lowIndex),
		buffer.getFloat (closeIndex),
		open_Time,
		close_Time,
		buffer.getLong (maxTimeIndex),
		buffer.getLong (minTimeIndex),
		buffer.getInt (ticksIndex),
		buffer.getLong (pointerIndex)
		
		)
		
	}
	
}

class Candle(val open:Float,
             val high:Float,
             val low:Float,
             val close:Float,
             val openTime:Long,
             val closeTime:Long,
             val maxTime:Long,
             val minTime:Long,
             val ticks:Int,
             val Ticks_before:Long
            ) {

	
	
	def getBytes = {
		val toReturn = ByteBuffer.allocate(Candle.byteLength)
		toReturn.putLong(Candle.openTimeIndex, openTime)
		toReturn.putFloat(Candle.openIndex, open)
		toReturn.putFloat(Candle.highIndex, high)
		toReturn.putFloat(Candle.lowIndex, low)
		toReturn.putFloat(Candle.closeIndex, close)
		toReturn.putLong(Candle.pointerIndex, Ticks_before)
		toReturn.putInt(Candle.ticksIndex, ticks)
		toReturn.putLong(Candle.maxTimeIndex, maxTime)
		toReturn.putLong(Candle.minTimeIndex, minTime)
		toReturn.array
	}
	def add_Tick(tk:Tick):Candle=
	{
		if(openTime<= tk.dateTime&& tk.dateTime<= closeTime)
			{
				val new_minTime=if((tk.bid+tk.ask)/2 <= low ) tk.dateTime else minTime
				val new_maxTime=if((tk.bid+tk.ask)/2 >= high ) tk.dateTime else maxTime
				new Candle(
					open,
					high.max((tk.bid+tk.ask)/2),
					low.min((tk.bid+tk.ask)/2),
					(tk.bid+tk.ask)/2,
					openTime,
					closeTime,
					new_maxTime,
					new_minTime,
					ticks+1,
					Ticks_before)
			}
		else this
	}
	
	def getTicks = ticks
	
	def getTicks_before = Ticks_before
	
	def getHigh = high
	
	def getLow = low
	
	def getClose = close
	
	def getOpenTime = openTime
	
	def getCloseTime = closeTime
	
	def getMaxTime = maxTime
	
	def getMinTime = minTime
	
	override def toString = String.valueOf(openTime) + "," + String.valueOf(open) + "," + String.valueOf(high) + "," + String.valueOf(low) + "," + String.valueOf(close) + "," + String.valueOf(Ticks_before) + "," + String.valueOf(ticks) + "," + String.valueOf(maxTime) + "," + String.valueOf(minTime)
	
	
}