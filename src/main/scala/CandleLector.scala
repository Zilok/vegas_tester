package algoTrading.Tester.kernel

import java.io.{File, IOException, RandomAccessFile}


import algoTrading.velas.kernel._
object  CandleLector{
	def get_vela_length(candle_Length: Int,units:String):Long=if (units.toLowerCase.startsWith("milis"))  candle_Length.toLong
	else if (units.toLowerCase.startsWith("sec")) candle_Length.toLong * 1000
	else if (units.toLowerCase.startsWith("min")) candle_Length.toLong * 1000 * 60
	else if (units.toLowerCase.startsWith("hou")) candle_Length.toLong * 1000 * 60 * 60
	else if (units.toLowerCase.startsWith("day")) candle_Length.toLong * 1000 * 60 * 60 * 24
	else if (units.toLowerCase.startsWith("wee")) candle_Length.toLong * 1000 * 60 * 60 * 24 * 7
	else if (units.toLowerCase.startsWith("mon")) candle_Length.toLong * 1000 * 60 * 60 * 24 * 30
	else 1L
	def apply(paridad: String, unit: String, candleLength: Int, tickreader: SynchronizedTickReader, dirOut: String):CandleLector=
	{
		val toRead = new File(dirOut + paridad + "_" + String.valueOf(candleLength) + unit + ".dat")
		if (!toRead.exists) throw new IOException("el Archivo " + toRead.getName + " no existe")
		//println(toRead)
		val len:Long=CandleLector.get_vela_length(candleLength,unit)
		val reader = new RandomAccessFile(toRead, "r")
		val buffer = new Array[Byte](reader.length().toInt)
		reader.readFully(buffer)
		//val candles_to_be_read=reader.length().toInt/Candle.byteLength
		val tmpArray: Array[Candle]=buffer
			.sliding(Candle.byteLength,Candle.byteLength)
	    .map(Candle(_,len))
			.toArray
	  	.sortWith(_.openTime<=_.openTime)
		val candles_read=tmpArray.length
	//	println(s"expected $candles_to_be_read read $candles_read")
		reader.close()
		new CandleLector(tickreader,paridad,unit,candleLength,tmpArray,dirOut)
		
	}
}
class CandleLector(val tickreader: SynchronizedTickReader,
                   val paridad: String,
                   val unit: String,
                   val candleLength: Int,
                   val velas: Array[Candle],
                   val dirOut: String
									 ) extends Iterator[Candle]
{

	private var _vela_Actual:Int=0
	lazy val cantVelas: Int=velas.length
	lazy val velaLength:Long=CandleLector.get_vela_length(candleLength,unit)
	val tickReader:TickLector = new TickLector(tickreader)
	
	def get_num_vela_Actual=_vela_Actual
	def vela_Actual_= (value:Int):Unit = _vela_Actual = value




	def getVelas = velas
	def getSymbol = paridad
	
	def getVela(i: Int) =  velas(0.max(_vela_Actual - i))
	def actual:Candle = if(0<= _vela_Actual&& _vela_Actual<cantVelas) velas(_vela_Actual)
		else
		{
			reset
			actual
		}
	override def next:Candle =
	{
		_vela_Actual += 1
		if(0<_vela_Actual&& _vela_Actual<=cantVelas)		velas(_vela_Actual-1)
		else
			{
				reset
				next
			}
	}
	override def hasNext = _vela_Actual < cantVelas - 2
	
	def reset:Unit = _vela_Actual = 0
	
	private def getTickReader(num: Int):TickLector =
	{
		tickReader.setPointer(Tick.byteLength * velas(num).getTicks_before)
		tickReader
	}
	
	def getTickReader:TickLector  =
		if (_vela_Actual < 0)             getTickReader(0)
		else if (_vela_Actual>=cantVelas) getTickReader(cantVelas-1)
		else                            getTickReader(_vela_Actual)
	
	def getFirstTick:Tick  =
		if (_vela_Actual < 0)             getFirstTick(0)
		else if (_vela_Actual>=cantVelas) getFirstTick(cantVelas-1)
		else                            getFirstTick(_vela_Actual)
	
	private def getFirstTick(num: Int) =
		if (num < 0)             getTickReader(0).next
		else if (num>=cantVelas) getTickReader(cantVelas-1).next
		else	                   getTickReader(num).next
	
	/**
		* Sets the pointer to the actual candle counter "velActual" to a number such that
		* velas(_vela_Actual-1).getOpenTime< nuT <= velas(_vela_Actual).getOpenTime
		* @param nuT the objective time
		*/
	def setDate(nuT: Long):Unit =
	{
		val tiempoObj = nuT - nuT % velaLength
		val delta = ((tiempoObj - actual.getOpenTime) / velaLength).toInt
		if (delta != 0)
			_vela_Actual=
				if (delta < 0)
					setDate_iterator(0.max( _vela_Actual + delta),_vela_Actual)
				else
					setDate_iterator(_vela_Actual,(cantVelas-1).min( _vela_Actual + delta))
		
		def setDate_iterator(banda_inferior:Int,banda_superior:Int):Int=
		{
			val mid_index:Int = (banda_superior + banda_inferior) / 2
			if ((banda_superior - 1 <= banda_inferior)||  (velas(banda_superior).getOpenTime == tiempoObj))
				banda_superior
			else if (velas(banda_inferior).getOpenTime == tiempoObj)
				banda_inferior
			else if (velas(mid_index).getOpenTime == tiempoObj)
				mid_index
			else if (velas(mid_index).getOpenTime < tiempoObj)
				setDate_iterator(mid_index,banda_superior)
			else
				setDate_iterator(banda_inferior,mid_index)
		}
	}
}