package algoTrading.Tester.kernel

import algoTrading.vegas.kernel.point
import algoTrading.velas.kernel._
import org.apache.commons.math3.util.FastMath

/**
	* @author Nicolas SG
	*
	*/
object Order
{
	val stopLossSTR = "Cause of close: Stop Loss"
	val trailStopLossSTR: String = "Cause of close: Trail Stop"
	val takeProfitSTR: String = "Cause of close: TakeProfit"
	val vueltaCanelaSTR: String = "Cause of close: vuelta Canela"
}

abstract class Order (val paridad: String, val signalTick: Tick, val  defaultStopLoss: Float, val defaultTakeProfit: Float, volume: Float, val isBuy: Boolean, val order_id: Int)
{
	val threshold_time:Long=12l*3600l*1000l
	val timeRequest = signalTick.getDateTime
	val isSell:Boolean = !isBuy
	val priceRequest = if (isBuy) signalTick.getAsk else signalTick.getBid
	val timeFilled = signalTick.getDateTime
	val priceFilled = if (isBuy) signalTick.getAsk	else signalTick.getBid
	
	def PYGpips=if (isBuy) closeFilled - priceFilled	else priceFilled - closeFilled
	def PyGPercentage = if (isBuy) - FastMath.log(priceFilled / closeFilled).toFloat else FastMath.log(priceFilled / closeFilled).toFloat
	def MaxProfitPIPS = if (isBuy) maxPrice - priceFilled	else priceFilled - minPrice
	def MaxProfitPerc =  if (isBuy) FastMath.log(maxPrice / priceFilled).toFloat	else FastMath.log(priceFilled / minPrice).toFloat
	def MaxDDPIPS =if (isBuy) priceFilled - minPrice	else maxPrice - priceFilled
	def MaxDDperc =  if (!isBuy) FastMath.log(maxPrice / priceFilled).toFloat	else FastMath.log(priceFilled / minPrice).toFloat
	
	protected var takeProfit=defaultTakeProfit
	protected var stopLoss= defaultStopLoss
	protected var maxPrice = priceFilled
	protected var minPrice = priceFilled
	protected var comment = ""
	protected var _isOpen = true
	protected var closeRequest = .0
	protected var closeFilled = .0
	protected var timeClosed = 0L
	protected var timeCloseRequest = 0L
	private var actualizo_en_primera_vela=false
	private var ultima_vela_que_actualizo=0
	
	//devuelve true si toca entrar en detalle
	// segundo nivel de velas, la actual deber?a ser una vela de detalle
	def update(actualtCandle: Candle ):Boolean =
	{
		var testigo = false //no  hay que entrar en detalle
		if (checkStop(actualtCandle, true)) return true // toca entrar en detalle si es tickby tick, de lo contrario ya habr? cerrado
		testigo = trailingStop(actualtCandle, true)
		if (!testigo&& (actualtCandle.closeTime-timeRequest<threshold_time)) { // no hay que entrar en detalle
			if (maxPrice < actualtCandle.getHigh) { // en caso tick by tick aqui posbilemente da salida stopLoss
				maxPrice = actualtCandle.getHigh
			}
			if (actualtCandle.getLow < minPrice) minPrice = actualtCandle.getLow
		}
		testigo
	}
	def isOpen=_isOpen
	def update_to_close(candles:CandleLector,details: CandleLector,first_index:Int,last_index:Int): Unit =
	{
		assert(candles.velas(first_index).getOpenTime<=timeFilled && timeFilled< candles.velas(first_index).closeTime,s" the first candle ${candles.velas(first_index)}  should open before $timeFilled")
		for (n <- first_index until last_index) update(details,candles.velas(n))
		if (_isOpen) 
		{
			candles.vela_Actual_=( last_index)
			val tick_to_close=candles.getFirstTick
			stop (tick_to_close, if (isBuy) tick_to_close.ask else tick_to_close.bid ,Order.vueltaCanelaSTR)
		}
	}
	
	//**
	// Esta es la primera vez que hace update, va a buscar en que subvela debe entrar en detalle
	//
	//*
	def update(details: CandleLector, actualtCandle: Candle ):Boolean = {
		if(_isOpen)
		{

			actualizo_en_primera_vela=actualizo_en_primera_vela|| actualtCandle.openTime <= timeFilled && timeFilled<actualtCandle.closeTime
			assert(actualizo_en_primera_vela,s"this is probably the first candle to update $actualtCandle, and time filled is $timeFilled")
			var testigo = false
			details.setDate(actualtCandle.getOpenTime)
			assert(actualtCandle.getOpenTime<= details.actual.getOpenTime && details.actual.getCloseTime <= actualtCandle.closeTime, s"details ${details.actual} should be inside $actualtCandle")
			var nextCandle = details.actual
			while ( {	nextCandle.getOpenTime < actualtCandle.getCloseTime && _isOpen})
			{
				
				testigo = update(nextCandle) //segundo nivel de update
				if (_isOpen && testigo ) { // la orden está abierta y hay que entrar en detalle
					update(details.getTickReader, nextCandle.getTicks) //tercer nivel de update
				}
				nextCandle = details.next
			}
		}
		_isOpen
	}
	
	def update(actualCandleTicks: TickLector, ticks: Int):Boolean = {
		var contador = 0
		val pointer = actualCandleTicks.getPointer
		val tk = actualCandleTicks.next(ticks)
		while ( {
			actualCandleTicks.hasNext && _isOpen && contador < tk.length
		}) {
			val actualTick = tk(contador)
			val midp = (actualTick.getAsk + actualTick.getBid) / 2
			trailingStop(actualTick)
			if(actualTick.dateTime-timeRequest<threshold_time)
				{
					maxPrice = Math.max(maxPrice, midp)
					minPrice = Math.min(minPrice, midp)
				}

			val causeOfStop = checkStop(actualTick.getBid, actualTick.getAsk, actualTick.getDateTime, true)
			if (causeOfStop < 0) {
				val str = if (this.stopLoss == this.defaultStopLoss) Order.stopLossSTR
				else Order.trailStopLossSTR
				return stop(actualTick, this.stopLoss, str)
			}
			if (causeOfStop > 0) return stop(actualTick, this.takeProfit, Order.takeProfitSTR)
			contador += 1
		}
		actualCandleTicks.setPointer(pointer)
		_isOpen
	}
	
	def stop(price: Float, closeTime: Long, new_comment: String) =
	{
		if (maxPrice < price) maxPrice = price
		if (price < minPrice) minPrice = price
		closeRequest = price
		closeFilled = price
		if (closeTime == 0) {
			timeCloseRequest = timeFilled
			timeClosed = timeFilled
			this.comment = comment + "hora de cierre incierta"
		}
		else {
			timeCloseRequest = closeTime
			timeClosed = closeTime
		}
		_isOpen = false
		comment = comment + new_comment
		_isOpen
	}
	
	def stop(t: Tick, price: Float, new_comment: String) =
	{
		val midp =t.getMidP
		if (maxPrice < midp) maxPrice = midp
		if (midp < minPrice) minPrice = midp
		closeRequest = price //(isSell)?t.getAsk():t.getBid();
		timeCloseRequest = t.getDateTime
		timeClosed = t.getDateTime
		closeFilled = if(isBuy)t.getBid else t.getAsk
		_isOpen = false
		comment = comment + new_comment
		_isOpen
	}
	
	
	def checkStop(thisCandle: Candle, TickByTick: Boolean):Boolean = {
		if (_isOpen) {
			val testigoUp = checkStop(thisCandle.getLow, thisCandle.getLow, thisCandle.getMinTime, TickByTick)
			val testigoDown = checkStop(thisCandle.getHigh, thisCandle.getHigh, thisCandle.getMaxTime, TickByTick)
			return !(testigoUp == 0 && testigoDown == 0)
		}
		false
	}
	
	/**
		*
		* @param bid
		* @param ask
		* @param timeSignal
		* @param TickByTick
		* @return -1 if close by stop 1 if close by tp 0 if neither
		*/
	def checkStop(bid: Float, ask: Float, timeSignal: Long, TickByTick: Boolean): Int = {
		val actualPrice = if (isBuy) bid		else ask
		if (stopLoss > 0 && _isOpen)
		{
			if (isSell) if (stopLoss <= actualPrice)
			{
				if (!TickByTick)
				{
					val str = if (stopLoss == defaultStopLoss) Order.stopLossSTR		else Order.trailStopLossSTR
					stop(stopLoss, timeSignal, str)
				}
				return -1
			}
			if (isBuy) if (stopLoss >= actualPrice)
			{
				if (!TickByTick)
				{
					val str = if (stopLoss == defaultStopLoss) Order.stopLossSTR		else Order.trailStopLossSTR
					stop(stopLoss, timeSignal, str)
				}
				return -1
			}
		}
		if (takeProfit > 0 && _isOpen)
		{
			if (isSell) if (takeProfit >= actualPrice)
			{
				if (!TickByTick) stop(takeProfit, timeSignal, Order.takeProfitSTR)
				return 1
			}
			if (isBuy) if (takeProfit <= actualPrice)
			{
				if (!TickByTick) stop(takeProfit, timeSignal, Order.takeProfitSTR)
				return 1
			}
		}
		0
	}
	
	def trailingStop(lastCandle: Candle, tickByTick: Boolean): Boolean
	
	def trailingStop(lastTick: Tick): Boolean
	
	def getStringRepresentation: String
	
	def getHeader: String
	
	def getTSparams: String
}