package algoTrading.Tester.kernel
import algoTrading.vegas.kernel.point
import org.apache.commons.math3.util.FastMath

object Stats {
	val year2009 = 1230786000000L
	val year2010 = 1262322000000L
	val year2011 = 1293858000000L
	val year2012 = 1325394000000L
	val year2013 = 1357016400000L
	val year2014 = 1388552400000L
	val year2015 = 1420088400000L
	val year2016 = 1451624400000L
	val year2017 = 1483246800000L
	val year2018 = 1514782800000L
	val year2019 = 1546318800000L
	val year2020 = 1577854800000L
	val min_year=year2010
	val max_year=year2018// any thing before 2018 but not in 2018
	val min_year_index=getYearIndex(min_year)
	val max_year_index=getYearIndex(max_year)-1
	val min_valid_prr:Double=0.1d
	val max_valid_prr:Double=10d

	
	 def getYearIndex(date: Long):Int =
		date match
		{
			case x if x < Stats.year2009=> 0 //Año 2008 y antes
			case x if x < Stats.year2010=> 1 //Año 2009
			case x if x < Stats.year2011=> 2 //Año 2010
			case x if x < Stats.year2012=> 3 //Año 2011
			case x if x < Stats.year2013=> 4 //Año 2012
			case x if x < Stats.year2014=> 5 //Año 2013
			case x if x < Stats.year2015=> 6 //Año 2014
			case x if x < Stats.year2016=> 7 //Año 2015
			case x if x < Stats.year2017=> 8 //Año 2016
			case x if x < Stats.year2018=> 9 //Año 2017
			case x if x < Stats.year2019=> 10 //Año 2018
			case x if x < Stats.year2020=> 11 //Año 2019
			case _ =>12
			//A?o 2020 em adelante
		}

	/**
		*
		* @param col list of orders
		* @return a pair,(U,V) where U is a sequence of results (MFE, PyG_pips) and V is a map of ( index_year,Seq [(MFE, PyG_pips)]) all sequences are sorted by year.
		*/
	def Order_to_Trade_seq(col: Iterable[Order]): (Seq[Order],Map[Int,Seq[Order]]) =
	{
		val tmp_trade_hist_YoY=col.filter(x=>min_year<= x.timeFilled && x.timeFilled<max_year)
			.toSeq
			.groupBy(x=>Stats.getYearIndex(x.timeFilled))
			.mapValues(_.sortWith(_.timeFilled<_.timeFilled))

		(col.toSeq.sortWith(_.timeFilled<_.timeFilled),tmp_trade_hist_YoY.filter(x=>min_year_index<=x._1 && x._1 <=max_year_index ))

	}
	private def getSigma(toTest:  Traversable[Double]):Double = {
		val xBar=getAvg(toTest)
		FastMath.sqrt(
			toTest
				.map(x=>(x-xBar)*(x-xBar))
				.sum
		)
	}
	def median_value(T: Traversable[Double]):Double=
	{
		if(T.isEmpty) 0
		else {
			val (down, up) = T.toSeq.sortWith(_ < _).splitAt(T.size / 2)
			if (T.size % 2 == 0) (down.last + up.head) / 2 else up.head
		}
	}
	def calculate_prr(N_win: Double, N_loss: Double, S_win: Double, S_loss: Double, M_win: Double, M_loss: Double, with_median:Boolean):Double=
	{
		val A_win=if(N_win==0) 1 else S_win/N_win
		val A_loss=if(N_loss==0) 1 else S_loss/N_loss
		val T=N_win+N_loss
		def prr_avg=((N_win+FastMath.sqrt(N_win)/T)/(N_loss+FastMath.sqrt(N_loss)/T  )) * (A_win/A_loss)
		def prr_median=((N_win+FastMath.sqrt(N_win)/T)/(N_loss+FastMath.sqrt(N_loss)/T  )) * (M_win/M_loss)
		def to_return(x:Double ):Double= if(x<min_valid_prr) min_valid_prr else if (x>max_valid_prr) max_valid_prr else x
		/**assert(prr_avg>0, s"(N_win, N_loss , S_win, S_loss, M_win, M_loss, prr_avg)= ($N_win, $N_loss , $S_win, $S_loss, $M_win, $M_loss, $prr_avg) " )
		assert(prr_median>0, s"(N_win, N_loss , S_win, S_loss, M_win, M_loss, prr_median)= ($N_win, $N_loss , $S_win, $S_loss, $M_win, $M_loss, $prr_median) " )*/
		if(with_median)	to_return(prr_median)
		else to_return(prr_avg)
	}
	def calculateAll(p:point, tradeHist : Seq[Order], trade_hist_YoY : Map[Int,Seq[Order]]): ((Double,Double,Double,Double,Double,Double,Double,Double,Double,Double),Double, Double,Double,Double,Double,(Double,Double,Double,Double,Double,Double,Double,Double)) = {

		/**
			*
			* @param accu the acumulator is a list of ( num wins, num loss, num trades, sum wins, sum loss, best run up, worst draw down, sum pyg, actual run up, actual draw down, last_date)
			* @param result actual trade result
			* @return the update after result to (
			*         (1) num wins,
			*         (2) num loss,
			*         (3) num trades,
			*         (4) sum wins,
			*         (5) sum loss,
			*         (6) best run up,
			*         (7) worst draw down,
			*         (8) sum pyg,
			*         (9) actual run up,
			*         (10) actual draw down,
			*         (11)last_date)
			*/
		def genStats(accu:(Double,Double,Double,Double,Double,Double,Double,Double,Double,Double,Long),result:Order):(Double,Double,Double,Double,Double,Double,Double,Double,Double,Double,Long)=
		{
			assert(accu._11<= result.timeFilled)
			val old_worst_draw_down=accu._7
			val old_best_run_up=accu._6
			val old_draw_down=accu._10
			val old_run_up=accu._9
			val actual_run_UP:Double=(old_run_up + result.PYGpips).max(0)
			val actual_draw_down:Double=(old_draw_down - result.PYGpips).max(0)
			val worst_draw_down= old_worst_draw_down.max(actual_draw_down)
			val best_run_up=old_best_run_up.max(actual_run_UP)

			if(result.PYGpips>0)
				(accu._1+1,accu._2,accu._3+1,accu._4+result.PYGpips,accu._5,best_run_up,worst_draw_down,accu._8+result.PYGpips,actual_run_UP,actual_draw_down,result.timeFilled)
			else
				(accu._1,accu._2+1,accu._3+1,accu._4,accu._5-result.PYGpips,best_run_up,worst_draw_down,accu._8+result.PYGpips,actual_run_UP,actual_draw_down,result.timeFilled)
		}
		def traversable_to_prr(trades:Traversable[Order]):Double=
		{
			lazy val wins = trades.map(_.PYGpips).filter(_ > 0)
			lazy val losses = trades.map(_.PYGpips).filterNot(_ > 0)
			lazy val num_wins = wins.size
			lazy val num_loss = losses.size
			lazy val sum_wins =  wins.sum
			lazy val sum_loss = losses.sum.abs
			lazy val median_win = median_value(wins)
			lazy val median_loss =  median_value(losses).abs

			if(trades.isEmpty) 0.98d*min_valid_prr
			else if(wins.isEmpty) 0.99d*min_valid_prr
			else if(losses.isEmpty) 1.1*max_valid_prr
			else  calculate_prr(num_wins, num_loss, sum_wins, sum_loss, median_win, median_loss, true)


		}

		val (num_wins, num_loss, num_trades, sum_wins, sum_loss, best_run_up, worst_draw_down, sum_PyG, actual_run_up, actual_draw_down,last_date)
		=tradeHist.foldLeft((0D,0D,0D,0D,0D,0D,0D,0D,0D,0D,0L))(genStats)

		val prrs_w_index=trade_hist_YoY.mapValues(traversable_to_prr)
		val index0=min_year_index// empezamos en 2010
		val prrs=(prrs_w_index(index0),prrs_w_index(index0+1),prrs_w_index(index0+2),prrs_w_index(index0+3),prrs_w_index(index0+4),prrs_w_index(index0+5),prrs_w_index(index0+6),prrs_w_index(index0+7))
		val min_prr=prrs_w_index.values.min

		val deviation_prr=getSigma(Seq(prrs_w_index(index0),prrs_w_index(index0+1),prrs_w_index(index0+2),prrs_w_index(index0+3),prrs_w_index(index0+4),prrs_w_index(index0+5),prrs_w_index(index0+6),prrs_w_index(index0+7)))
		val median_win=median_value(tradeHist.map(_.PYGpips)filter(_>0))
		val median_loss=median_value(tradeHist.map(_.PYGpips)filterNot(_>0)).abs
		val tmp_prr=calculate_prr(num_wins,num_loss,sum_wins,sum_loss,median_win,median_loss,true) //will return a number between 0.5 and 10

		assert(0<=deviation_prr && deviation_prr< 1000,s"deviation of prr is huge $deviation_prr")
		assert(min_prr>=0.8d*min_valid_prr, s"min prr is 0 $prrs_w_index")
	
		((num_wins, num_loss, num_trades, sum_wins, sum_loss, best_run_up, worst_draw_down, sum_PyG, actual_run_up, actual_draw_down),median_win,median_loss, deviation_prr,min_prr,tmp_prr,prrs)

	}
	
	 def getAvg(toTest: Traversable[Double]):Double = {
		toTest.sum/toTest.size.toDouble
	}
	
	def apply( p:point, col: Iterable[Order]):Stats=
		{
			val		(tradeHist , trade_hist_YoY) :( Seq[Order], Map[Int,Seq[Order]])=Order_to_Trade_seq(col)

			val ((num_wins, num_loss, num_trades, sum_wins, sum_loss, best_run_up, worst_draw_down, sum_PyG, actual_run_up, actual_draw_down),median_win,median_loss, deviation_prr,min_prr,tmp_prr,prrs)
			= calculateAll(p,tradeHist,trade_hist_YoY)
			val mfe=tradeHist.map(_.MaxProfitPIPS).sum.toDouble
			val prr2010 =prrs._1
			val prr2011 =prrs._2
			val prr2012=prrs._3
			val prr2013=prrs._4
			val prr2014=prrs._5
			val prr2015=prrs._6
			val prr2016=prrs._7
			val prr2017=prrs._8
			new Stats( worst_draw_down, best_run_up, num_wins, num_loss, num_trades, sum_wins, sum_loss,median_win,median_loss, tmp_prr,  sum_PyG,mfe, deviation_prr, min_prr, prr2010, prr2011, prr2012, prr2013, prr2014, prr2015, prr2016, prr2017)
		}
}

class Stats(
	           val  worstDrawDown : Double,
	           val  bestRunUp     : Double,
	           val  nWins         : Double,
	           val  nLoss         : Double,
	           val  numTrades     : Double,
	           val  sumWins       : Double,
	           val  sumLoss       : Double,
						 val median_win     : Double,
						 val median_loss    : Double,
	           val  PRR           : Double,
	           val  acumPYG       : Double,
						 val  MFE           : Double,
	           val  sigmaPrr      : Double,
	           val  MinPrr        : Double,
	           val  prr2010       : Double,
	           val  prr2011       : Double,
	           val  prr2012       : Double,
	           val  prr2013       : Double,
	           val  prr2014       : Double,
	           val  prr2015       : Double,
	           val  prr2016       : Double,
	           val  prr2017       : Double)
{
	lazy val stat1:Double = acumPYG / worstDrawDown
	lazy val stat2:Double = PRR
	lazy val stat3:Double = (acumPYG / worstDrawDown) * nWins / nLoss
	lazy val stat4:Double = (acumPYG / worstDrawDown) * PRR
	lazy val stat5:Double = acumPYG / (worstDrawDown * sigmaPrr)
	lazy val stat6:Double = List((prr2017,1),(prr2016,2),(prr2015,3),(prr2014,4),(prr2013,5),(prr2012,6),(prr2011,7),(prr2010,8))

.map(x=>(x._1*FastMath.pow(0.9d,x._2)) ).sum/( (1 to 8).map(FastMath.pow(0.9d,_)).sum)
	lazy val stat7:Double= if(acumPYG>0) (nWins * median_win)/(nLoss * median_loss) else -(nWins * median_win)/(nLoss * median_loss)

}