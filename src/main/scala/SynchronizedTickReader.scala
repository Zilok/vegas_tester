package algoTrading.Tester.kernel

import java.io.{File, RandomAccessFile}

import algoTrading.velas.kernel._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await,  Future}
class SynchronizedTickReader (val Symbol: String, val dirOut: String) {
	val file_name:String=dirOut + Symbol + ".dat"
	/*val toReadFile = new File(file_name)
	val reader = new RandomAccessFile(toReadFile, "r")
	val fileLength:Long = reader.length
	var pointer:Long=0L
	var isReading = false

	//def tickIndex:Long=pointer/Tick.byteLength.toLong*/
	def  length = new File(file_name).length()
	
	
	private def readticks(number_of_ticks:Int, nu_pointer:Long):	Future[(Array[Tick],Long)]=
	{
		import scala.concurrent.ExecutionContext.Implicits.global
		//implicit val ec = ExecutionContext .fromExecutor(Executors.newFixedThreadPool(1))
		Future {
				scala.concurrent.blocking({
					val toReadFile = new File(file_name)
					assert(number_of_ticks.toLong*Tick.byteLength.toLong<Int.MaxValue.toLong,s"Reading many ticks $number_of_ticks exceeds integer size ")
					assert(nu_pointer+number_of_ticks.toLong*Tick.byteLength.toLong<toReadFile.length(),s"Reading many ticks $number_of_ticks exceeds file length")
					val  number_of_bytes:Int=number_of_ticks*Tick.byteLength
					val buff = new Array[Byte](number_of_bytes)
					//val initial_pointer=nu_pointer
					val reader = new RandomAccessFile(toReadFile, "r")
					reader.seek(nu_pointer)
					reader.readFully(buff)
					val pointer=reader.getFilePointer
					reader.close()
					assert(nu_pointer.toLong+number_of_ticks.toLong*Tick.byteLength.toLong==pointer,s" ${nu_pointer.toLong+number_of_ticks.toLong*Tick.byteLength.toLong} is not equal to ${reader.getFilePointer}" )
					val to_return=buff
						.sliding(Tick.byteLength,Tick.byteLength)//.toVector.par
						.map(Tick(_))
					(to_return.toArray,pointer)
				}
				)
		}
	}
	
	//def getPointer = pointer
	//def getTick:Tick =readticks(1).head


	def getTick(numTicks: Int, nu_pointer:Long):(Array[Tick],Long)=
		{

			Await.result(readticks(numTicks,nu_pointer) ,Duration.Inf)
		}
	
	//def getTick(numTicks: Int): Array[Tick] =readticks(numTicks)
	/*
	private def setPointer(point: Long) =
	{
			if (point < Tick.byteLength) pointer = 0
			else if (point % Tick.byteLength != 0) pointer = point - point % Tick.byteLength
			else if (point > length - Tick.byteLength) pointer = length - Tick.byteLength
			else pointer = point
		
		pointer
	}*/
	
	/*def AquireLock(id: point):Boolean = synchronized
	{

		while (isReading && !id.compare(idUser) ) wait()


		if (idUser.compare(default_user) && !isReading)
			{
				isReading=true
				idUser = id
			}
		else return false
		isReading
	}
	
	
	
	def ReleaseLock() = synchronized  {
		
		isReading=false
		idUser = default_user
		notifyAll()
	}*/
}