package algoTrading.velas.kernel

import java.nio.ByteBuffer
import java.sql.Timestamp
import java.text.{ParseException, SimpleDateFormat}

object Tick {
	val byteLength = java.lang.Long.BYTES + 2 * java.lang.Float.BYTES
	
	def StringToTime(str: String):Long = try if (str.contains(":")) new Timestamp(new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS").parse(str).getTime).getTime
	else if (str.contains(" ")) new Timestamp(new SimpleDateFormat("yyyyMMdd HHmmssSSS").parse(str).getTime).getTime
	else java.lang.Long.parseLong(str)
	catch {
		case e: ParseException =>
			System.out.println(e.getMessage)
			0L
	}
	
	def isWellFormated(str: String):Boolean = {
		val subStrings = str.split(",")
		val size = subStrings.length
		var timeIndex = 0
		var bidIndex = 1
		var askIndex = 2
		if (size >= 3) try {
			if (str.contains("/")) {
				timeIndex = 1
				bidIndex = 2
				askIndex = 3
			}
			if (str.contains(":")) new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS").parse(subStrings(timeIndex))
			else if (str.contains(" ")) new SimpleDateFormat("yyyyMMdd HHmmssSSS").parse(subStrings(timeIndex))
			else java.lang.Long.parseLong(subStrings(timeIndex))
			
			val bid = subStrings(bidIndex).toFloat
			val ask = subStrings(askIndex).toFloat
			if (ask < bid)            return false
			if (ask <= 0 || bid <= 0) return false
		} catch {
			case ex: Exception =>
				ex.printStackTrace()
				return false
		}
		else return false
		true
	}
	
	def apply(str: String): Tick =
	{		
		val (timeIndex ,bidIndex ,askIndex)=	if (str.contains("/")) (1,2,3) else (0,1,2)
		val strs = str.split(",")
		val tmp_bid=strs(bidIndex).toFloat
		val tmp_ask=strs(askIndex).toFloat
		if ( tmp_bid==0||tmp_ask==0)
			println(str)
		new Tick(StringToTime(strs(timeIndex)),tmp_bid,tmp_ask )
	}	
		
	
	def apply(bytes: Array[Byte]):Tick=
	{
		val buffer = ByteBuffer.wrap(bytes)
		new Tick(buffer.getLong(0),
			buffer.getFloat(java.lang.Long.BYTES),
			buffer.getFloat(java.lang.Long.BYTES + java.lang.Float.BYTES))
	}
	
}

class Tick(val dateTime:Long, val bid:Float, val ask:Float )
{
	assert(bid>0, s"non positive bid value: $bid ")
	assert(ask>0, s"non positive ask value: $ask ")
	assert(bid<=ask,s"bid: $bid greater than ask: $ask ")
	def getDateTime = dateTime
	def getBid = bid
	def getAsk = ask
	def getMidP= (bid+ask)/2
	def getBytes: Array[Byte] = {
		val toReturn = ByteBuffer.allocate(Tick.byteLength)
		toReturn.putLong(0, dateTime)
		toReturn.putFloat(java.lang.Long.BYTES, bid)
		toReturn.putFloat(java.lang.Long.BYTES + java.lang.Float.BYTES, ask)
		toReturn.array
	}
	
	override def toString = {
		val numLength = 10
		val timeLength = 13
		var strReprs:String = ""
		val strTime = String.valueOf(dateTime) //new java.text.SimpleDateFormat("yyyyMMdd HHmmssSSS").format(new java.sql.Timestamp(dateTime));
		if (strTime.length != timeLength) System.out.println("tick with no Time out of range:  " + java.time.Instant.ofEpochMilli(dateTime).toString)
		var strBid = String.valueOf(bid)
		var strAsk = String.valueOf(ask)
		val zero = "00000000000000"
		var asklen = strAsk.length
		var bidlen = strBid.length
		if (asklen > numLength || bidlen > numLength) {
			val testBid = bid > 1
			val testAsk = ask > 1
			val decimals = if (ask > 10) 3
			else 5
			val pip = if (ask > 10) 0.01.toFloat
			else 0.0001.toFloat
			val tmpBid = Math.rint(bid / (pip * 0.1.toFloat)).toInt
			val tmpAsk = Math.rint(ask / (pip * 0.1.toFloat)).toInt
			val actBidPoint = strBid.indexOf('.')
			val actAskPoint = strAsk.indexOf('.')
			strBid = String.valueOf(tmpBid)
			strBid = if (testBid) strBid.substring(0, actBidPoint) + "." + strBid.substring(actBidPoint, actBidPoint + decimals)
			else "0." + strBid
			strAsk = String.valueOf(tmpAsk)
			strAsk = if (testAsk) strAsk.substring(0, actAskPoint) + "." + strAsk.substring(actAskPoint, actAskPoint + decimals)
			else "0." + strAsk
			asklen = strAsk.length
			bidlen = strBid.length
		}
		strBid = if (strBid.indexOf('.') == -(1)) strBid + "." + zero.substring(0, numLength - 1 - bidlen)
		else strBid + zero.substring(0, numLength - bidlen)
		strAsk = if (strAsk.indexOf('.') == -(1)) strAsk + "." + zero.substring(0, numLength - 1 - asklen)
		else strAsk + zero.substring(0, numLength - asklen)
		strReprs = strTime + "," + strBid + "," + strAsk
		strReprs
	}
}