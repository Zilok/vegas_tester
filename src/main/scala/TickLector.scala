package algoTrading.Tester.kernel

import java.util
import algoTrading.velas.kernel._

import scala.concurrent.duration.Duration
import scala.concurrent.Await
class TickLector(val reader: SynchronizedTickReader) extends util.Iterator[Tick] {

	private val readerLength = reader.length
	private var pointer = 0L


	
	override def hasNext:Boolean = pointer < readerLength - Tick.byteLength
	
	override def next:Tick = getTick(1).head
	def next(numTicks: Int) = getTick(numTicks)
	
	
	private def getTick(numTicks: Int):Array[Tick] = {
		val Actualpointer = pointer
		//import scala.concurrent.ExecutionContext.Implicits.global
		//val res:Future[(Array[Tick],Long)]=reader.getTick(numTicks,Actualpointer)
		val toReturn=reader.getTick(numTicks,Actualpointer)
		pointer=toReturn._2
		assert(Actualpointer + numTicks.toLong * Tick.byteLength.toLong == toReturn._2, s"initilal pointer: $Actualpointer, numTicks to read: $numTicks, byteLenth ${Tick.byteLength}, expected result: ${Actualpointer + numTicks.toLong * Tick.byteLength.toLong }, resulting pointer $pointer")
		toReturn._1

	}
	
	def setPointer(point: Long) = {
		if (point > readerLength) {
			println(s"dodgy stuff going on $point, $readerLength")
			pointer = readerLength - Tick.byteLength
		}
		pointer = point
	}
	
	def getPointer = pointer
}