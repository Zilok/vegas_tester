package algoTrading.vegas.kernel

import collection.JavaConverters._
import org.apache.commons.math3.util.FastMath
import org.datavec.api.records.reader.RecordReader
import org.deeplearning4j.eval.RegressionEvaluation
import org.datavec.api.records.reader.impl.csv.CSVRecordReader
import org.datavec.api.split.FileSplit
import org.nd4j.linalg.io.ClassPathResource
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator
import org.nd4j.evaluation.classification.Evaluation
import org.deeplearning4j.nn.conf.MultiLayerConfiguration
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.layers.DenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer
import org.deeplearning4j.nn.conf.layers.Layer
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.{DataSet, api}
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.learning.config.Nesterovs
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator
import java.io.File
import java.io.{File, FileWriter}
import java.time.format.DateTimeFormatter
import java.time.{Duration, Instant}
import java.{lang, util}

import algoTrading.Tester.kernel.{CandleLector, Stats, SynchronizedTickReader}
import algoTrading.velas.kernel.Tick
import org.apache.commons.math3.linear.{Array2DRowRealMatrix, ArrayRealVector, LUDecomposition, RealMatrix}
import org.apache.commons.math3.util.FastMath
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.dataset.api.DataSetPreProcessor
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator
import org.nd4j.linalg.factory.Nd4j

import scala.annotation.tailrec
import scala.collection.immutable
import scala.collection.parallel.immutable.ParVector
import scala.concurrent.{Await, Future}


object VegasOptimizer {

	def getGrid(radius:Int=640,base:Int=10, center: point_range=point_range(650,655,1,0), random:Boolean=false): ParVector[point_range] =
	{
		lazy val step=2*radius/base
		lazy val new_radius=step*base/2
		lazy val min_Ema_Fast=(center.fast_ema-new_radius).max(10)
		lazy val min_Ema_Slow=(center.slow_ema-new_radius).max(15)
		lazy val max_Ema_Fast=min_Ema_Fast+2*new_radius
		
		lazy val deterministic_grid: ParVector[point_range]=(min_Ema_Fast until  max_Ema_Fast by step)
			.flatMap(x=> (min_Ema_Slow.max(x+5) until min_Ema_Slow.max(x+5)+2*new_radius by step)
				. map(y=> point_range(x,y,center.zona_ts,center.stop)  ) )
	  	.toVector.par
		lazy val random_grid: ParVector[point_range]=(1 to base*base).map(x=>
		{
		def gen_emas():(Int, Int)=
		{
			val ema1= FastMath.round((radius+center.fast_ema)*FastMath.random()).toInt.max(10)
			val ema2= FastMath.round((radius+center.slow_ema)*FastMath.random()).toInt.max(15)
			if ( ema1<ema2) 	(ema1,ema2)
			else if (ema2<ema1) (ema2,ema1)
			else gen_emas
		}
			val (ema1,ema2)=gen_emas()
			
			point_range(ema1,ema2,center.zona_ts,center.stop)
		}).sortWith((p1,p2)=>p1.fast_ema<p2.fast_ema ||(p1.fast_ema==p2.fast_ema && p1.slow_ema< p2.slow_ema) ).toVector.par
		if(random) random_grid
		else deterministic_grid
	}
	def getParams(acum:(String, Int,Int,Int),to_parse: List[String]):(String, Int,Int,Int)=
	{
		to_parse match
		{
			case Nil => acum
			case param::not_parsed=>
			{
				param match
				{
					case x if x.toLowerCase.endsWith("symbol") => getParams((instrumentos(not_parsed.head.toInt),acum._2,acum._3,acum._4 ), not_parsed.tail)
					case x if x.toLowerCase.endsWith("zona")   => getParams((acum._1,acum._2,acum._3,not_parsed.head.toInt), not_parsed.tail)
					case x if x.toLowerCase.endsWith("ts")     => getParams((acum._1,acum._2,not_parsed.head.toInt,acum._4 ), not_parsed.tail)
					case x if x.toLowerCase.endsWith("radio")  => getParams((acum._1,not_parsed.head.toInt,acum._3,acum._4 ), not_parsed.tail)
					case _=> getParams(acum, not_parsed)
				}
			}
		}
	}
	def main(args: Array[String]):Unit =
	{
		val (instrumento, radio, zona_ts, ts  ):(String, Int,Int,Int)= getParams(("EURUSD",640,1,-1),args.toList)
		val tick_Dir="""C:\fxdataOUT\"""
		val result_Dir="D:\\AlgoTrading\\Resultados\\"
		val actual_zona=0//1//zona_ts
		val actual_radius=640//radio
		val optimizer= new VegasOptimizer(instrumento,1,tick_Dir,result_Dir,result_Dir)
		//val done=optimizer.load_stats(actual_zona)
		optimizer.calc_NN(point(10,15,22,100))
		//optimizer.get_best_ts(point(10,1273,22,100))
		//(1 to 5).flatMap(x=> optimizer.find_optimum(x,radio,done))
		//optimizer.execute_queue(ParVector( point(652,1428,18,0).to_point_range ) )

		//val results=optimizer.find_optimum(actual_zona,actual_radius,done) //done.reduce(optimizer.best_of_queue_reducer(0))//
		//val best: (point, VegasStats) =results
		//println(best._1)
		/*val ts_list: Seq[(point, Seq[(Int,Int)])] =Vector(point(10,1177,131,200),
			point(10,1410,131,200),
			point(12,1175,131,200),
			point(18,1545,131,200),
			point(32,1519,131,200),
			point(34,1450,131,200),
			point(50,1482,131,200),
			point(66,1557,131,200),
			point(122,175,131,200),
			point(138,859,131,200),
			point(161,1018,131,200),
			point(202,1458,131,200),
			point(289,954,131,200),
			point(330,876,131,200),
			point(366,1467,131,200),
			point(439,489,131,200),
			point(471,1237,131,200),
			point(536,1189,131,200),
			point(544,1185,131,200),
			point(591,637,131,200),
			point(599,860,131,200),
			point(604,1197,131,200)
		)
			.map(x=>(x,optimizer.get_best_ts(x)))
		ts_list.foreach(x=> {
				val values: List[Int] =x._2.map(_._2).toList

				def ts_function_1(ts: List[Int])(channel: Double): Int = {
					assert(channel > 0)
					val index = (channel / channel_width).floor.toInt
					if (index < ts.length)
						ts(index)
					else
						ts(ts.length - 1)

				}

				def ts_function: (Double) => Int = (channel) => ts_function_1(values)(channel)

				(50 to 350 by 10).map(point(x._1.fast_ema,x._1.slow_ema, 131, _)).toVector.par
					.map(optimizer.execute_variable_ts(ts_function)).toList.sortWith((point_Stat1, point_Stat2) => (point_Stat1._1.fast_ema < point_Stat2._1.fast_ema) || ((point_Stat1._1.fast_ema == point_Stat2._1.fast_ema) && (point_Stat1._1.slow_ema < point_Stat2._1.slow_ema)))
					.foreach(point_Stat => {
						val writer = new FileWriter(optimizer.StatsFiles(6), true)
						writer.write(point_Stat._2.getResults + "\r\n")
						writer.close()
					})
			}
		)
		ts_list.foreach(println)


		def  ts_function: ( Double)=>Int =  ( chanel )=>
		{
			val lim_ts0:Double=0.00320910173468291
			val lim_ts1:Double=0.00641987053677439
			val lim_ts2:Double=0.00773812225088477
			val lim_ts3:Double=0.0100991474464535
			val lim_ts4:Double=0.0158415660262107
			val lim_ts5:Double=0.019880685955286
			val lim_ts6:Double=0.0251877922564744
			val lim_ts7:Double=0.0318317413330078
			val val_ts0:Int=77
			val val_ts1:Int=47
			val val_ts2:Int=11
			val val_ts3:Int=31
			val val_ts4:Int=73
			val val_ts5:Int=11
			val val_ts6:Int=31
			val val_ts7:Int=21
			chanel match {
				case x if x <= lim_ts0 => val_ts0
				case x if lim_ts0 < x && x <= lim_ts1 => val_ts1
				case x if lim_ts1 < x && x <= lim_ts2 => val_ts2
				case x if lim_ts2 < x && x <= lim_ts3 => val_ts3
				case x if lim_ts3 < x && x <= lim_ts4 => val_ts4
				case x if lim_ts4 < x && x <= lim_ts5 => val_ts5
				case x if lim_ts5 < x && x <= lim_ts6 => val_ts6
				case x if lim_ts6 < x  => val_ts7
			}
		}
*/


	}
	val channel_width: Double =0.003d
	lazy val  numofThreads = Runtime.getRuntime.availableProcessors
	def instrumentos(index:Int):String = index match
		{
		case 0=>"AUDUSD"
		case 1=>"EURUSD"
		case 2=>"GBPUSD"
		case 3=>"USDCAD"
		case 4=>"XAUUSD"
		case 5=>"XAGUSD"
		case 6=>"NZDUSD"
		case 7=>"USDJPY"
		case 8=>"EURAUD"
		case 9=>"EURNZD"
		case 10=>"GBPAUD"
		case 11=>"GBPNZD"
		case 12=>"AUDJPY"
		case 13=>"NZDJPY"
		case 14=>"EURJPY"
		case 15=>"GBPJPY"
		case 16=>"CADJPY"
		case 17=>"AUDCAD"
		case 18=>"NZDCAD"
		case 19=>"EURCAD"
		case 20=>"GBPCAD"
		case 21=>"XAUGBP"
		case 22=>"XAUEUR"
		case 23=>"XAGGBP"
		case 24=>"XAGEUR"
		case 25=>"USDMXN"
		case _=>"EURUSD"
	 }
		
	
	def TimeStamp = {
		val formatter = DateTimeFormatter.ofPattern("d MMM yyyy HH:mm:ss.SSS")
		"[" + //(new SimpleDateFormat("HH.mm.ss.SSS")).format(new Date())+
			formatter.format(java.time.ZonedDateTime.ofInstant(java.time.Instant.now, java.time.ZoneId.systemDefault)) + "]"
	}
	
	def zonaTS(ts: Int):(Int,Int) =
		if (ts==0)        (0,0)   // no trail stop
		else if (ts == 1) (1,21) // larga
		else if (ts == 2) (22,78)  // media
		else if (ts == 3) (79,102)
		else if (ts == 4) (103,123)  // corta
		else if (ts == 5) (124,130) // corta modificada
		else               (1,130)
	
	def zonaTS2(ts: Int): Int =
		if      (ts==0)                  0
		else if (1 <= ts && ts <= 21)    1
		else if (22 <= ts && ts <= 78)   2
		else if (79 <= ts && ts <= 102)  3
		else if (103 <= ts && ts <= 123) 4
		else if (124 <= ts && ts <= 130) 5
		else 6
	
	
}
case class point(val fast_ema:Int, slow_ema:Int, ts:Int, stop: Int)
{
	assert(fast_ema<slow_ema,s"fast ema bigger $fast_ema than $slow_ema")
	assert(fast_ema>=10, s"$fast_ema with too small")
	assert(slow_ema>=15, s"$slow_ema with too small")
	assert(stop!=0, s"stop can't be 0 $stop")
	assert(stop>0, s"stop should be positive $stop")
	assert(stop<1000, s"stop should be less than 10% $stop")
	assert(0<=ts && ts <=131, s"trail stop out of range $ts")
	override def  toString= s"$fast_ema;$slow_ema;$ts;$stop"
	def to_point_range()=point_range(fast_ema,slow_ema,VegasOptimizer.zonaTS2(ts),stop)
	def compare(that:point):Boolean= 0==((fast_ema-that.fast_ema ).abs+(slow_ema-that.slow_ema ).abs +(ts-that.ts).abs+ (stop-that.stop).abs)
}
case class point_range(val fast_ema:Int, slow_ema:Int, zona_ts:Int, stop: Int)
{
	assert(fast_ema<slow_ema,s"fast ema bigger $fast_ema than $slow_ema")
	assert(0<= zona_ts && zona_ts <= 6, s" ts zone $zona_ts out of range")
	assert(fast_ema>=10, s"$fast_ema with too small")
	assert(slow_ema>=15, s"$slow_ema with too small")
	assert(stop!=0, s"stop can't be 0 $stop")
	assert(stop>0, s"stop should be positive $stop")
	assert(stop<1000, s"stop should be less than 10% $stop")
	lazy val (ts1,ts2)=VegasOptimizer.zonaTS(zona_ts)
	override def  toString= s"$fast_ema;$slow_ema;$zona_ts;$stop"
}
class VegasOptimizer(val symbol: String, val tsZona: Int, val dir_In: String, val dir_detailed_results:String, val dir_stats:String  )
{
	def  tick_reader = new SynchronizedTickReader(symbol, dir_In)
	val min_option= 0
	val max_option=7
	def StatsFiles(ts:Int): File ={
			val stats_file = new File(dir_stats + symbol + String.valueOf(Math.abs(ts)) + ".csv")
			import java.io.FileWriter
			if (!stats_file.exists) {
				if (!stats_file.getParentFile.exists) stats_file.getParentFile.mkdirs
				stats_file.createNewFile
				val writer = new FileWriter(stats_file, true)
				writer.write(VegasStats.getHeaderResults+ "\r\n")
				writer.close()
			}
			stats_file
		}

	lazy val queue_filename:String=dir_stats+symbol+"_actual_queue.csv"
	lazy val ts_knn_filename:String=dir_stats+"""\"""+symbol+"""_knn\ts_knn_results"""
	def queue_file() =
		{
			val f=new File(queue_filename)
			if (!f.exists) {
				if (!f.getParentFile.exists) f.getParentFile.mkdirs
				f.createNewFile()
			}
			f
		}
	def ts_knn_file(ema_fast:Int, ema_slow:Int, kmeansKernels:Int)=
	{
		val f=new File(ts_knn_filename+'_'+kmeansKernels.toString+'_'+ema_fast.toString+'_'+ema_slow.toString+".csv")
		if (!f.exists) {
			if (!f.getParentFile.exists) f.getParentFile.mkdirs
			f.createNewFile()
		}
		f
	}

	def load_queue():ParVector[point_range]= scala.io.Source.fromFile(queue_file)
				.getLines.toVector.par
		  	.map(str=>
				  {
					  val parts=str.split(";")
					  point_range(parts(0).toInt,parts(1).toInt,parts(2).toInt,parts(3).toInt)
				  })
	def save_queue(to_do:ParVector[point_range]):Unit=
	{
		val writer = new FileWriter(	 queue_file(),  false)
		to_do.toVector
	  	.foreach(p=>writer.write(s"$p"+ "\r\n") )

		writer.close()
	}

	def execute_queue(to_Do:ParVector[point_range], done:ParVector[(point,VegasStats)] =ParVector[(point,VegasStats)]()):ParVector[(point, VegasStats)]=
	{
		def point_to_stats(param:point_range):ParVector[(point,VegasStats)]=
		{

			lazy val candle_reader_1H: CandleLector = CandleLector(symbol, "Hour", 1, tick_reader, dir_In)
			lazy val candle_reader_5m: CandleLector = CandleLector(symbol, "minutes", 5, tick_reader, dir_In)
			VegasTester
				.velas_to_orders(candle_reader_1H, candle_reader_5m,  param)
				.map(x=>
					{
						import scala.concurrent.ExecutionContext.Implicits.global
						val toRet=Future {
							val this_Point = point(param.fast_ema, param.slow_ema, x._1, param.stop)
							val point_Stat = (this_Point, VegasStats(x._1, param.fast_ema, param.slow_ema, param.stop, x._2))
							val writer = new FileWriter(StatsFiles(param.zona_ts ), true)
							writer.write(point_Stat._2.getResults + "\r\n")
							writer.close()
							if (point_Stat._2.stat2 > 1)
								VegasTester.print_orders(x._2, this_Point, symbol, dir_detailed_results)
							point_Stat
						}
						Await.result(toRet,scala.concurrent.duration.Duration.Inf)
					}).par
		}

		val queue=
			if(done.isEmpty)
				to_Do
			else
			to_Do
				.filterNot(x=>done.map(y=> y._1.to_point_range()==x ).reduce(_||_)	)
		import scala.collection.parallel._
		queue.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(VegasOptimizer.numofThreads))
		save_queue(queue)
		val t0=Instant.now
		val new_Done: ParVector[(point, VegasStats)] =queue.flatMap(point_to_stats)++done
		val t1=Instant.now()
		val diff=Duration.between(t0,t1)
		println(VegasTester.TimeStamp+s" Ended Queue of size ${queue.length}  in $diff")
		new_Done
	}
	def load_stats(): ParVector[(point, VegasStats)] =
	{
		(0 to 5).map(StatsFiles).flatMap(f=>
			scala.io.Source.fromFile(f)
				.getLines.toSeq.tail
		  	.map(VegasStats(_))
		  	.map(stat=>(point(stat.getPeriod_EmaFast,stat.getPeriod_EmaSlow,stat.getTSOption,stat.getStop),stat) )
		).toVector.par

	}
	def load_stats(ts:Int): ParVector[(point, VegasStats)] =
	{
		scala.io.Source.fromFile(StatsFiles(ts))
			.getLines.toSeq.tail
			.map(VegasStats(_))
			.map(stat=>(point(stat.getPeriod_EmaFast,stat.getPeriod_EmaSlow,stat.getTSOption,stat.getStop),stat) )
	  	.toVector
	  	.par
	}



	def line_Optimization(origin:(point,VegasStats),option:Int, done:ParVector[(point,VegasStats)]):ParVector[(point,VegasStats)]=
	{

		val queue1: ParVector[point_range] =VegasOptimizer.
			getGrid(6,3,origin._1.to_point_range())

		val points_tested1=execute_queue(queue1,done)
		val points=points_tested1.filter(x=>queue1.exists(p=> p==x._1.to_point_range()) && x._1.ts==origin._1.ts )
		println(VegasTester.TimeStamp ++ s" total stats calculated ${points_tested1.length} total points to be in gradient aproximation ${points.size}")

		/**
			* The tangent plane to a curve at (x0,y0,f(x0,y0)) can be written as
			* f_x(x0,y0)(x-x0)+f_y(x0,y0)(y-y0)=z-z0
			* hence
			* f_x(x0,y0)x+f_y(x0,y0)y+z0-f_x(x0,y0)x0-f_y(x0,y0)y0 =z
			* so we are doing the regression of the plane
			* A(x-x0)+B(y-y0)=z-z0
			* finally the vector
			* <A,B>
			* is an estimation of the gradient vector of the curve f(x,y)
			*
			* Points coordenadas en EmaFast, EmaSlow para hacer regresi?n
			*  option función objetivo
			*               =1 ->  AcumPYG/WDD
			*               =2 ->  PRR
			*               =3 -> (AcumPYG/WDD)*nWIns/nLoss
			*               =4 -> (AcumPYG/WDD)*PRR
			*               =5 -> AcumPYG/(WDD*sigmaPRR )
			*               default ->  PRR
			*
			*/
		val A: Array[Array[Double]] =points.map(p=>Array(p._1.fast_ema-origin._1.fast_ema,p._1.slow_ema-origin._1.slow_ema).map(_.toDouble)).toArray
		val b:Array[Double]=points.map(p=> option match
		{
			case 0 => p._2.MFE-origin._2.MFE
			case 1 => p._2.stat1-origin._2.stat1
			case 2 => p._2.stat2-origin._2.stat2
			case 3 => p._2.stat3-origin._2.stat3
			case 4 => p._2.stat4-origin._2.stat4
			case 5 => p._2.stat5-origin._2.stat5
			case 6 => p._2.stat6-origin._2.stat6
			case 7 => p._2.stat7-origin._2.stat7
			case _ =>p._2.PRR -origin._2.PRR
		}).toArray
		val leastSquares: Array2DRowRealMatrix = new Array2DRowRealMatrix(A)
		val leastSquaresSolved: RealMatrix = (new LUDecomposition(leastSquares.transpose.multiply(leastSquares)))
			.getSolver
			.getInverse
			.multiply(leastSquares.transpose)
		val result=new ArrayRealVector(leastSquaresSolved.operate(b)).toArray.toVector
		val norm=math.sqrt(result.map(x=>x*x).sum)
		println(VegasTester.TimeStamp+ s" gradient at ${origin._1} is $result with prr of ${origin._2.PRR} for option $option")
		if (norm< 0.001)
		{
			println( s" norma pequeña ${origin._1}")
			if  (min_option<= option && option < max_option) line_Optimization(points_tested1.reduce(best_of_queue_reducer(option+1)),option+1, points_tested1)
			else points_tested1
		}
		else
			{
				val line=(-50 to 50 by 5).map(x=>
				{
					def gen_emas():(Int, Int)=
					{
						val ema1:Int=(origin._1.fast_ema+(result(0)/norm)*x).round.toInt.max(10)
						val ema2:Int=(origin._1.slow_ema+(result(1)/norm)*x).round.toInt.max(15)
						if (ema1< ema2) (ema1,ema2)
						else if (ema1>ema2) (ema2,ema1)
						else (ema1,ema1+1)
					}
					val (ema1,ema2)=gen_emas

					point_range(ema1 , ema2 ,VegasOptimizer.zonaTS2(origin._1.ts),origin._1.stop)
				}
				).toVector.par
				//line.foreach(println)
				val points_tested2=execute_queue(line, points_tested1)

				val new_best=points_tested2.reduce(best_of_queue_reducer(option))
				if(new_best._1!=origin._1) line_Optimization(new_best,option, points_tested2)
				else if (min_option <=option && option < max_option) line_Optimization(points_tested2.reduce(best_of_queue_reducer(option+1)),option+1, points_tested2)
				else points_tested2
			}

	}

	def best_of_queue_reducer(option:Int)(A1:(point,VegasStats),A2:(point,VegasStats)):(point,VegasStats)= option match
		{
			case 0 => if( A1._2.MFE< A2._2.MFE) A2 else A1
			case 1 => if( A1._2.stat1< A2._2.stat1) A2 else A1
			case 2 => if( A1._2.stat2< A2._2.stat2) A2 else A1
			case 3 => if( A1._2.stat3< A2._2.stat3) A2 else A1
			case 4 => if( A1._2.stat4< A2._2.stat4) A2 else A1
			case 5 => if( A1._2.stat5< A2._2.stat5) A2 else A1
			case 6 => if( A1._2.stat6< A2._2.stat6) A2 else A1
			case 7 => if( A1._2.stat7< A2._2.stat7) A2 else A1
			case _ => if( A1._2.PRR< A2._2.PRR) A2 else A1
		}



	def execute_variable_ts(ts_function: Function1[ Double,Int])(test_point : point ): (point, VegasStats) =
	{
		assert(test_point.ts==131)
		lazy val candle_reader_1H: CandleLector = CandleLector(symbol, "Hour", 1, tick_reader, dir_In)
		lazy val candle_reader_5m: CandleLector = CandleLector(symbol, "minutes", 5, tick_reader, dir_In)

		val results=VegasTester.velas_to_orders(candle_reader_1H,	candle_reader_5m,	test_point,	ts_function	)
		VegasTester.print_orders(results,test_point,symbol,dir_detailed_results)
		(test_point, VegasStats(6, test_point.fast_ema, test_point.slow_ema, test_point.stop, results))
	}

	def find_optimum(ts_zone:Int=1, radius:Int=640, initial_done:ParVector[(point,VegasStats)]):Seq[(point, VegasStats)] =
	{

			val stop_inicial=100
			println(VegasTester.TimeStamp+s" beginning of grid of radius $radius")
			val initial_Queue=
				if (initial_done.isEmpty) VegasOptimizer.getGrid(radius,10,point_range(radius,radius+10,ts_zone,stop_inicial),random=true)
				else (min_option to max_option).map(x=> initial_done.reduce(best_of_queue_reducer(x))).flatMap(y=>VegasOptimizer.getGrid(radius,10,y._1.to_point_range)).toVector.par.distinct

			val Done_1=execute_queue(initial_Queue,initial_done)
			val Done_2=line_Optimization(Done_1.reduce(best_of_queue_reducer(min_option)),min_option,Done_1)
			if(radius>20) find_optimum(ts_zone,radius/2, Done_2)
			else min_option to max_option map (x=> Done_2.reduce(best_of_queue_reducer(x)))


	}

def preprocess(p:point )=
	{
		val points=calculate_all_ts(p)
		val buy_points=points.filter(_._1)
		val sell_points=points.filterNot(_._1)
		val N_buy:Int=FastMath.round(buy_points.length*0.9/100).toInt*100
		val N_sell:Int=FastMath.round(sell_points.length*0.9/100).toInt*100
		val (buy_train,buy_test)=buy_points.splitAt(N_buy)
		val (sell_train,sell_test)=buy_points.splitAt(N_sell)
		val buy_means: Array[Float] =buy_train
			.map(x =>	x._2)
			.reduce((A,B)=>A.zip(B).map(x=>x._1+x._2))
  		.map(_/N_buy)
		val sell_means: Array[Float]=sell_train
			.map(x =>	x._2)
			.reduce((A,B)=>A.zip(B).map(x=>x._1+x._2))
			.map(_/N_sell)
		val buy_devs: Array[Float] =buy_train
			.map(x =>	x._2.zip(buy_means).map{case(x,x_bar)=>
				val intermediate=(x-x_bar)
				intermediate*intermediate
			} )
			.reduce((A,B)=>A.zip(B).map(x=>x._1+x._2))
			.map(_/(N_buy-1))
  		.map(FastMath.sqrt(_).toFloat)
		val sell_devs: Array[Float] =sell_train
			.map(x =>	x._2.zip(sell_means).map{case(x,x_bar)=>
				val intermediate=(x-x_bar)
				intermediate*intermediate
			} )
			.reduce((A,B)=>A.zip(B).map(x=>x._1+x._2))
			.map(_/(N_sell-1))
			.map(FastMath.sqrt(_).toFloat)

		val buy_train_norm=buy_train.map(x=>
			x._2
				.zip(buy_means)
				.map{case(a,b)=>a-b}
				.zip(buy_devs)
				.map{case(a,b)=>a/b}
		)
		val buy_test_norm=buy_test.map(x=>
			x._2
				.zip(buy_means)
				.map{case(a,b)=>a-b}
				.zip(buy_devs)
				.map{case(a,b)=>a/b}
		)
		val sell_train_norm=sell_train.map(x=>
			x._2
				.zip(sell_means)
				.map{case(a,b)=>a-b}
				.zip(sell_devs)
				.map{case(a,b)=>a/b}
		)
		val sell_test_norm=sell_test.map(x=>
			x._2
				.zip(sell_means)
				.map{case(a,b)=>a-b}
				.zip(sell_devs)
				.map{case(a,b)=>a/b}
		)

		((buy_points.map(_._2),buy_train_norm,buy_test_norm,buy_means,buy_devs),
			(sell_points.map(_._2),sell_train_norm,sell_test_norm,sell_means,sell_devs))
	}
	def calc_NN(p:point )
	=
	{
		val (buy_Data,sell_Data)= preprocess(p:point )
		val buy_train_vars: INDArray =Nd4j.create(buy_Data._2.map(_.drop(2)))
		val buy_train_mfe: INDArray =Nd4j.create(buy_Data._2.map(_.take(1)))
		val buy_train_mae: INDArray =Nd4j.create(buy_Data._2.map(_.slice(1,2)))
		val sell_train_vars: INDArray =Nd4j.create(sell_Data._2.map(_.drop(2)))
		val sell_train_mfe: INDArray =Nd4j.create(sell_Data._2.map(_.take(1)))
		val sell_train_mae: INDArray =Nd4j.create(sell_Data._2.map(_.slice(1,2)))
		val buy_mfe_Data=new DataSet(buy_train_vars,buy_train_mfe)
		val sell_mfe_Data=new DataSet(sell_train_vars,sell_train_mfe)
		val buy_mae_Data=new DataSet(buy_train_vars,buy_train_mae)
		val sell_mae_Data=new DataSet(sell_train_vars,sell_train_mae)


		val seed:Int=1026252// java.lang.System.currentTimeMillis.toInt
		val learningRate:Double=0.01
		val batchSize:Int =512
		val nMinEpochs:Int=100
		val nEpochs:Int=100
		val numInputs:Int=11
		val numOutputs:Int =1
		val numHiddenNodes1:Int = 30
		val numHiddenNodes2:Int = 30
		val numHiddenNodes3:Int = 30
		val numHiddenNodes4:Int = 30
		val numHiddenNodes5:Int = 30
		val numHiddenNodes6:Int = 30
		val numHiddenNodes7:Int = 30
		val numHiddenNodes8:Int = 30
		val numHiddenNodes9:Int = 30
		val numHiddenNodes10:Int = 30
		val numHiddenNodes11:Int = 30
		val numHiddenNodes12:Int = 30
		val numHiddenNodes13:Int = 30
		val numHiddenNodes14:Int = 30
		val numHiddenNodes15:Int = 30
		val fast_dim_nodes=Array(numInputs,numHiddenNodes1,numHiddenNodes2,numHiddenNodes3,numHiddenNodes4,numHiddenNodes5,numHiddenNodes6,numHiddenNodes7,numOutputs)
		val activations=Array(simple_speedy_net.RELU,simple_speedy_net.RELU,simple_speedy_net.RELU,simple_speedy_net.RELU,simple_speedy_net.RELU,simple_speedy_net.RELU,simple_speedy_net.RELU,simple_speedy_net.IDENTITY)

		buy_mfe_Data.batchBy(batchSize)
		sell_mfe_Data.batchBy(batchSize)
		buy_mae_Data.batchBy(batchSize)
		sell_mae_Data.batchBy(batchSize)

		val conf = new NeuralNetConfiguration.Builder()
  		.seed(seed)
			.weightInit(WeightInit.XAVIER)
			.updater(new Nesterovs(learningRate, 0.9))
			.list()
			.layer(new DenseLayer.Builder()
				.nIn(numInputs)
				.nOut(numHiddenNodes1)
				.activation(Activation.RELU)//SOFTSIGN
				.build())
			.layer(new DenseLayer.Builder()
				.nIn(numHiddenNodes1)
				.nOut(numHiddenNodes2)
				.activation(Activation.RELU)
				.build())
			.layer(new DenseLayer.Builder()
			.nIn(numHiddenNodes2)
			.nOut(numHiddenNodes3)
			.activation(Activation.RELU)
			.build())
			.layer(new DenseLayer.Builder()
				.nIn(numHiddenNodes3)
				.nOut(numHiddenNodes4)
				.activation(Activation.RELU)
				.build())
			.layer(new DenseLayer.Builder()
				.nIn(numHiddenNodes4)
				.nOut(numHiddenNodes5)
				.activation(Activation.RELU)
				.build())
			.layer(new DenseLayer.Builder()
				.nIn(numHiddenNodes5)
				.nOut(numHiddenNodes6)
				.activation(Activation.RELU)
				.build())
			.layer(new DenseLayer.Builder()
				.nIn(numHiddenNodes6)
				.nOut(numHiddenNodes7)
				.activation(Activation.RELU)
				.build())
			.layer(new OutputLayer.Builder(LossFunction.SQUARED_LOSS)
				.nIn(numHiddenNodes7)
				.nOut(numOutputs)
				.activation(Activation.IDENTITY)
				.build())
			.build()

		val model_buy_mfe = new MultiLayerNetwork(conf)
		model_buy_mfe.init()
		model_buy_mfe.setListeners(new ScoreIterationListener(10))
		model_buy_mfe.fit( buy_mfe_Data)


		var n=0
		var score=2d
		while(n<nMinEpochs||( n<nEpochs && score>0.02))
		{
			model_buy_mfe.fit( buy_mfe_Data)
			val grad_score=model_buy_mfe.gradientAndScore()
			score=grad_score.getSecond
			if(n%100==0) println(s"for epoch $n the buy mfe score is ${grad_score.getSecond}")
			n=n+1
		}
		println(s"for epoch $n the buy mfe score is ${score}")
		/*n=0
		score=2d
		val model_sell_mfe=model_buy_mfe.clone()

		val model_buy_mae=model_buy_mfe.clone()
		while(n<nMinEpochs||( n<nEpochs && score>0.02))
		{
			model_sell_mfe.fit( sell_mfe_Data)
			val grad_score=model_sell_mfe.gradientAndScore()
			score=grad_score.getSecond
			if(n%100==0) println(s"for epoch $n the sell mfe score is ${grad_score.getSecond}")
			n=n+1
		}
		println(s"for epoch $n the  score is ${score}")
		n=0
		score=2d
		while(n<nMinEpochs||( n<nEpochs && score>0.02))
		{
			model_buy_mae.fit( buy_mae_Data)
			val grad_score=model_buy_mae.gradientAndScore()
			score=grad_score.getSecond
			if(n%100==0) println(s"for epoch $n the buy mae score is ${grad_score.getSecond}")
			n=n+1
		}
		println(s"for epoch $n the  score is ${score}")
		n=0
		score=2d
		val model_sell_mae=model_buy_mae.clone()
		while(n<nMinEpochs||( n<nEpochs && score>0.02))
		{
			model_sell_mae.fit( sell_mae_Data)
			val grad_score=model_sell_mae.gradientAndScore()
			score=grad_score.getSecond
			if(n%100==0) println(s"for epoch $n the sell mae score is ${grad_score.getSecond}")
			n=n+1
		}
		println(s"for epoch $n the  score is ${score}")
		*/
		val buy_mfe_table=model_buy_mfe.paramTable().asScala
		val b_buy_mfe: Array[Float] =buy_mfe_table("0_b").toFloatVector++
			buy_mfe_table("1_b").toFloatVector++
			buy_mfe_table("2_b").toFloatVector++
			buy_mfe_table("3_b").toFloatVector++
			buy_mfe_table("4_b").toFloatVector++
			buy_mfe_table("5_b").toFloatVector++
			buy_mfe_table("6_b").toFloatVector++
			buy_mfe_table("7_b").toFloatVector

		val w_buy_mfe:Array[Float] =
			(buy_mfe_table("0_W").transpose.toFloatMatrix.map(_.toVector).toVector.flatten ++
			buy_mfe_table("1_W").transpose.toFloatMatrix.map(_.toVector).toVector.flatten ++
			buy_mfe_table("2_W").transpose.toFloatMatrix.map(_.toVector).toVector.flatten ++
			buy_mfe_table("3_W").transpose.toFloatMatrix.map(_.toVector).toVector.flatten ++
			buy_mfe_table("4_W").transpose.toFloatMatrix.map(_.toVector).toVector.flatten ++
			buy_mfe_table("5_W").transpose.toFloatMatrix.map(_.toVector).toVector.flatten ++
			buy_mfe_table("6_W").transpose.toFloatMatrix.map(_.toVector).toVector.flatten ++
			buy_mfe_table("7_W").toFloatVector.toVector).toArray
		//class simple_speedy_net(W:Array[Float], b:Array[Float], hidden_dims:Array[Int],activations:Array[Int], num_layers:Int) {
		val test_speed_nn= new simple_speedy_net(w_buy_mfe, b_buy_mfe,fast_dim_nodes ,activations,8)

		val t0=Instant.now()
		buy_Data._2.foreach(vec=>
		{

			val test_output=test_speed_nn.feedForward(vec.drop(2))

			//val train_output=model_buy_mfe.output(Nd4j.create(vec.drop(2)),false)

		})
		val t1=Instant.now()

		buy_Data._2.foreach(vec=>
		{

			//val test_output=test_speed_nn.feedForward(vec.drop(2))

			val train_output=model_buy_mfe.output(Nd4j.create(vec.drop(2)),false)

		})
		val t2=Instant.now()
		//println(s"test output: ${test_output.head}; in ${Duration.between(t0,t1)} train output $train_output in ${Duration.between(t1,t2)}; expected output:${vec.take(1).head} ")
		println(s"custom NN in ${Duration.between(t0,t1)} Nd4j in ${Duration.between(t1,t2)} for  ${buy_Data._2.length} samples")

	}




	def load_strat_hist(symbol:String,params:point):ParVector[(Double,Double,Double,Double)]
	=
	{
		val fileName = symbol +s"""${VegasOptimizer.zonaTS2(params.ts)}\\"""+symbol+s"""${params.fast_ema.toString}-${ params.slow_ema}-ts${params.ts}-stop${params.stop}.csv"""
		val to_read=new File(fileName)
		if(to_read.exists())
		{
			scala.io.Source.fromFile(to_read)
				.getLines.toVector.tail.par
				.map(str=>
				{
					val parts=str.split(';')
					lazy val id= parts(0).toInt
					lazy val time_Open= Tick.StringToTime(parts(1))
					lazy val time_Closed= Tick.StringToTime (parts(2))
					lazy val Tipo= parts(3).startsWith("Buy")
					lazy val price_Request= parts(4).toFloat
					lazy val close_Request= parts(5).toFloat
					lazy val maxPrice= parts(6).toFloat
					lazy val minPrice= parts(7).toFloat
					lazy val PYG_pips= parts(8).toFloat
					lazy val PyGPercentage= parts(9).toFloat
					lazy val Default_stop_Loss= parts(10).toFloat
					lazy val Trail_Loss= parts(11).toFloat
					lazy val MaxDDPIPS= parts(12).toFloat
					lazy val MaxDDperc= parts(13).toFloat
					lazy val MaxProfitPIPS= parts(14).toFloat
					lazy val MaxProfitPerc= parts(15).toFloat
					lazy val volume= parts(16).toFloat
					lazy val baseEma= parts(17).toFloat
					lazy val Ema_Fast= parts(18).toFloat
					lazy val Ema_Slow= parts(19).toFloat
					lazy val MACD_std= parts(20).toFloat
					lazy val MACD_implicit= parts(21).toFloat
					lazy val Signal_MACD_std= parts(22).toFloat
					lazy val Signal_MACD_implicit= parts(23).toFloat
					lazy val histogram_MACD_std= parts(24).toFloat
					lazy val histogram_MACD_implicit= parts(25).toFloat
					lazy val chanel= parts(26).toFloat
					lazy val entry= parts(27).toFloat
					lazy val comment= parts(28)
					(chanel.toDouble,PYG_pips.toDouble,MaxProfitPIPS.toDouble , params.ts.toDouble)

				}).filter(_._2>0)
		}
		else ParVector[(Double,Double,Double,Double)]()

	}
	def calculate_all_ts(p:point)//:Array[(Boolean,Array[Float],Array[Float],Array[Float])]
	=
	{
		println(VegasTester.TimeStamp+s" beginning calcs on $p for all ts")

		val param=point_range(p.fast_ema,p.slow_ema,0,p.stop )

		//val preloaded_results: Map[Int, ParVector[(Double, Double,Double, Double)]] =(1 to 130).map(ts=>(ts,load_strat_hist(symbol,  point(p.fast_ema,p.slow_ema,ts,p.stop) ))).filterNot(_._2.isEmpty).toMap
		val start=0
		val end=0
		val step=1
		val results  =(start to end by step  ).par.flatMap(
			x=> {
				lazy val candle_reader_1H: CandleLector = CandleLector(symbol, "Hour", 1, tick_reader, dir_In)
				lazy val candle_reader_5m: CandleLector = CandleLector(symbol, "minutes", 5, tick_reader, dir_In)


				VegasTester
					.velas_to_orders(candle_reader_1H, candle_reader_5m, param, x, x + step -1)
					.flatMap { case (ts, orders) =>
						VegasTester.print_orders(orders, point(p.fast_ema, p.slow_ema, ts, p.stop), symbol, dir_detailed_results)
						orders
							//.filter(_.PYGpips>0)
							.map(ord =>(
								ord.isBuy,
								Array(ord.MaxProfitPIPS,
								ord.MaxDDPIPS,
								ord.baseEma,
								ord.ema_Fast,
								ord.ema_Slow,
								ord.chanel,
								ord.entry,
								ord.MACD_standard,
								ord.Histogram_MACD_standard,
								ord.Signal_MACD_standard,
								ord.MACD_implicit,
								ord.Histogram_MACD_implicit,
								ord.Signal_MACD_implicit)
						)
						)
					}
			})
  			.toArray
		println(VegasTester.TimeStamp+s" done calcs on $p for al ts")

		results
	}
	/*will create groups of channels of 30 pips and find best ts for each channel will return the ts for each channel*/
	/*will use knn to find the best on each group*/

	/*def get_best_ts(p:point )
	=
	{

		val points=calculate_all_ts(p)
		val means= Point_Stat_utils.averageVectors(points)
		val devs=Point_Stat_utils.std_Dev_Vectors(points)
		val normal_data =points.map(_.renormalize(means,devs,0,1,to_new_mean = true)).toVector.par
		println(VegasTester.TimeStamp + s"there is a total of ${points.size} to classify")
		val kmeansKernels=100
		val max_iterations=10
		var t0=Instant.now
		var clusters1 =knn_classifier.getClusters(normal_data,kmeansKernels).filter(_._2._1>0)
		var i=0
		var j=0

		var condition=i<max_iterations
		var t1=Instant.now
		var dispertion_path=Vector[Double](max_iterations)
		println(VegasTester.TimeStamp +s"Finished iteration $i of $max_iterations in ${Duration.	between(t0,t1)}")
		while(condition)
		{
			t0=Instant.now
			i=i+1
			val old_k=clusters1.length.toDouble
			val actual_dispertion=clusters1.map(_._2._4).sum/old_k


			val clusters2=knn_classifier.getClusters(normal_data,kmeansKernels).filter(_._2._1>0)
			val new_k=clusters2.length.toDouble
			val new_dispertion=clusters2.map(_._2._4).sum/new_k

			clusters1= if(new_dispertion<actual_dispertion) clusters2 else clusters1

			condition=i<max_iterations
			t1=Instant.now
			println(VegasTester.TimeStamp+s"Finished iteration $i of $max_iterations in ${Duration.	between(t0,t1)} actual dispertion: $actual_dispertion---> $new_dispertion; num means: $old_k---> $new_k")
			dispertion_path=if( i==0) Vector(actual_dispertion,new_dispertion min actual_dispertion ) else dispertion_path:+(new_dispertion min actual_dispertion)
			val name_steadiness=10

			val condition_2:Boolean= if (i>name_steadiness) ! dispertion_path.takeRight(name_steadiness).foldLeft((true,dispertion_path.last))( (A,B)=>(A._1 && A._2==B,B))._1
			else true
			condition=condition&& condition_2

		}

		knn_classifier.results_printer(
			clusters1,
			points,
			means,
			devs,
			( new  XSSFWorkbook()).createSheet(s"resultados_${p.fast_ema}_${p.slow_ema}_${p.stop}"),
			ts_knn_filename+s"_${p.fast_ema}_${p.slow_ema}_${p.stop}.xlsx",
			Array(
				"nombre",
				"isBuy",
				"chanel",
				"entry",
				"MACD_standard",
				"MACD_implicit",
				"Histogram_MACD_standard",
				"Histogram_MACD_implicit",
			//	"PYGpips",
				"MaxProfitPIPS"//,
				//"ts"
			))
		clusters1


	}*/







}