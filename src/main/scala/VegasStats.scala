package algoTrading.vegas.kernel
import algoTrading.Tester.kernel.Stats.{Order_to_Trade_seq, calculateAll}
import algoTrading.Tester.kernel._


object VegasStats {
	def getHeaderResults =
		"period Ema Fast;period Ema Slow;option Trail Stop; stop; acum PyG;acum MFE;bestRunUp;worst Draw Down;PRR;num Trades;num Wins;num Loss;sum Wins;sum Loss;median Win;Median loss;sigma Prr;Min Prr;prr 2010;prr 2011;prr 2012;prr 2013;prr 2014;prr 2015;prr 2016;prr 2017;stat 1;stat 2;stat 3;stat 4;stat 5;stat 6;stat 7"

	def apply(str:String): VegasStats =
		{

			val parts=str.replaceAll(",",".").split(";")
			val new_period_EmaFast=  parts(0).toInt
			val new_period_EmaSlow=  parts(1).toInt
			val new_optionTS=        parts(2).toInt
			val new_stop=            parts(3).toFloat
			val new_acumPYG=         parts(4).toDouble
			val new_MFE=             parts(5).toDouble
			val new_bestRunUp=       parts(6).toDouble
			val new_worstDrawDown=   parts(7).toDouble
			val new_PRR=             parts(8).toDouble
			val new_numTrades=       parts(9).toDouble
			val new_nWins=           parts(10).toDouble
			val new_nLoss=           parts(11).toDouble
			val new_sumWins=         parts(12).toDouble
			val new_sumLoss=         parts(13).toDouble
			val new_median_Win=      parts(14).toDouble
			val new_Median_loss=     parts(15).toDouble
			val new_sigmaPrr=        parts(16).toDouble
			val new_MinPrr=          parts(17).toDouble
			val new_prr2010=         parts(18).toDouble
			val new_prr2011=         parts(19).toDouble
			val new_prr2012=         parts(20).toDouble
			val new_prr2013=         parts(21).toDouble
			val new_prr2014=         parts(22).toDouble
			val new_prr2015=         parts(23).toDouble
			val new_prr2016=         parts(24).toDouble
			val new_prr2017=         parts(25).toDouble
			new VegasStats(new_worstDrawDown, new_bestRunUp, new_nWins, new_nLoss, new_numTrades, new_sumWins, new_sumLoss, new_PRR, new_acumPYG,new_MFE, new_sigmaPrr, new_MinPrr,new_median_Win,new_Median_loss, new_optionTS, new_period_EmaFast, new_period_EmaSlow, new_stop, new_prr2010, new_prr2011, new_prr2012, new_prr2013, new_prr2014, new_prr2015, new_prr2016, new_prr2017)
		}
	
	def apply(  optionTS: Int, period_EmaFast: Int, period_EmaSlow: Int, stop: Float, col: Iterable[Order]): VegasStats =
	{
		val p=point(period_EmaFast,period_EmaSlow,optionTS,stop.toInt)
		lazy val		(tradeHist , trade_hist_YoY) :( Seq[Order], Map[Int,Seq[Order]])=Order_to_Trade_seq(col)
		lazy val ((num_wins, num_loss, num_trades, sum_wins, sum_loss, best_run_up, worst_draw_down, sum_PyG, actual_run_up, actual_draw_down),median_win,median_loss, deviation_prr,min_prr,tmp_prr,prrs)
		= calculateAll(p,tradeHist,trade_hist_YoY)
		lazy val mfe=tradeHist.map(_.MaxProfitPIPS).sum.toDouble
		lazy val prr2010 =prrs._1
		lazy val prr2011 =prrs._2
		lazy val prr2012=prrs._3
		lazy val prr2013=prrs._4
		lazy val prr2014=prrs._5
		lazy val prr2015=prrs._6
		lazy val prr2016=prrs._7
		lazy val prr2017=prrs._8
	
		
		
		new VegasStats( worst_draw_down, best_run_up, num_wins, num_loss, num_trades, sum_wins, sum_loss, tmp_prr, sum_PyG,mfe, deviation_prr, min_prr,median_win,median_loss, optionTS, period_EmaFast, period_EmaSlow, stop, prr2010, prr2011, prr2012, prr2013, prr2014, prr2015, prr2016, prr2017)
	}
}

class VegasStats(  worstDrawDown: Double, bestRunUp: Double, nWins: Double, nLoss: Double, numTrades: Double, sumWins: Double, sumLoss: Double, PRR: Double, acumPYG: Double,mfe:Double ,sigmaPrr: Double, MinPrr: Double,median_win:Double ,median_loss:Double , optionTS: Int, period_EmaFast: Int, period_EmaSlow: Int, stop: Float, prr2010: Double, prr2011: Double, prr2012: Double, prr2013: Double, prr2014: Double, prr2015: Double, prr2016: Double, prr2017: Double) extends Stats( worstDrawDown, bestRunUp, nWins, nLoss, numTrades, sumWins, sumLoss,median_loss,median_win, PRR, acumPYG,mfe, sigmaPrr, MinPrr, prr2010, prr2011, prr2012, prr2013, prr2014, prr2015, prr2016, prr2017)
{
	
	def getPeriod_EmaFast = period_EmaFast
	
	def getPeriod_EmaSlow = period_EmaSlow
	
	def getTSOption = optionTS
	
	def getStop = Math.floor(stop).toInt
	override def toString=getResults
	def getResults = {
		s"$period_EmaFast;$period_EmaSlow;$optionTS;$stop;$acumPYG;$mfe;$bestRunUp;$worstDrawDown;$PRR;$numTrades;$nWins;$nLoss;$sumWins;$sumLoss;$median_win;$median_loss;$sigmaPrr;$MinPrr;$prr2010;$prr2011;$prr2012;$prr2013;$prr2014;$prr2015;$prr2016;$prr2017;$stat1;$stat2;$stat3;$stat4;$stat5;$stat6;$stat7"
		.replace(".", ",")
	}
}