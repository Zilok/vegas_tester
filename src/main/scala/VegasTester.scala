package algoTrading.vegas.kernel
import java.io.File
import java.time.format.DateTimeFormatter

import algoTrading.Tester.kernel._
import algoTrading.velas.kernel.Tick
import org.apache.commons.math3.util.FastMath

import scala.collection.parallel.immutable.ParVector



object VegasTester {
	def TimeStamp = {
		val formatter = DateTimeFormatter.ofPattern("d MMM yyyy HH:mm:ss.SSS")
		"[" + formatter.format(java.time.ZonedDateTime.ofInstant(java.time.Instant.now, java.time.ZoneId.systemDefault)) + "]"
	}
	def Trade(buy: Boolean,  emaFast: Float, emaSlow: Float, MACDIstd: Float, MACDIimpl: Float, signalMACDIstd: Float, signalMACDIimpl: Float, HistogramMACDIstd: Float, HistogramMACDIimpl: Float, stopPerc:Float, optionTS1:Int, optionTS2:Int, lastTicks: TickLector, paridad:String,id:Int ,orders:Vector[VegasOrder]):Vector[VegasOrder] =
	{
		val (max_ema, min_ema) = if (emaFast < emaSlow) (emaSlow, emaFast) else (emaFast, emaSlow)
		val tk = lastTicks.next
		val price = if (buy) tk.getAsk else tk.getBid
		val entry: Float=if (buy) price - max_ema else min_ema - price
		val actualCH: Float= max_ema-min_ema

		for (x <- orders )
		{
			if(x.isOpen&& (x.isBuy!=buy))// hay ordenes abiertas, y la nueva orden es en dirección contraria a las anteriores
			{
				x.stop(tk, price, Order.vueltaCanelaSTR)
			}
		}
		
		
		val stop =
			if (stopPerc == 0) 0
			else if (buy) (FastMath.exp(-stopPerc / 10000) * price).toFloat
			else (FastMath.exp(stopPerc / 10000) * price).toFloat
		val new_orders:Vector[VegasOrder]=
			(optionTS1 to  optionTS2)
				.map(TMPts=>
					new VegasOrder(paridad,
						tk,
						stop, //stop
						//tp
						1000, //volume
						buy, //isBuy
						id, //id
						emaFast, // emaFast
						emaSlow, //ema slow
						MACDIstd,
						MACDIimpl,
						signalMACDIstd,
						signalMACDIimpl,
						HistogramMACDIstd,
						HistogramMACDIimpl,
						entry, // entry vs ema
						actualCH,
						TMPts)
				).toVector
		
		orders++new_orders
	}
	
	def velas_to_orders(
		                   cabeza_Lectora: CandleLector,
		                   cabeza_Lectora_Details: CandleLector,
											 test_point:point_range,option_TS_1:Int= -1,
											 option_TS_2:Int= -2)
	:Vector[(Int,Vector[VegasOrder])]={


		cabeza_Lectora.reset
		lazy val period_EmaFast:Int=test_point.fast_ema
		lazy val period_EmaSlow:Int=test_point.slow_ema
		lazy val option_Trail_Stop_1:Int= if(option_TS_1<1 || option_TS_1>130) test_point.ts1 else option_TS_1
		lazy val option_Trail_Stop_2:Int= if(option_TS_2<1 || option_TS_2>130|| option_TS_2<option_TS_1+2) test_point.ts2.max(option_TS_1) else option_TS_2
		lazy val stop_Percentage: Float= test_point.stop.toFloat
		lazy val alfa_Fast:Double=2d/(1d+period_EmaFast.toDouble)
		lazy val alfa_Slow:Double=2d/(1d+period_EmaSlow.toDouble)
		lazy val alfa_12 =2d/(1d+12d)// 0.153846153846153846d
		lazy val alfa_26 =2d/(1d+26d)// 0.074074074074074074d
		lazy val starter_MACD_standard_9_history:Map[Int, Double]=Map((0,0d),(1,0d),(2,0d),(3,0d),(4,0d),(5,0d),(6,0d),(7,0d),(8,0d))
		lazy val starter_MACD_implicit_9_history:Map[Int, Double]=Map((0,0d),(1,0d),(2,0d),(3,0d),(4,0d),(5,0d),(6,0d),(7,0d),(8,0d))
		lazy val min_Date = Stats.year2010
		//println(s"ts1 $option_Trail_Stop_1, ts2 $option_Trail_Stop_2, stop $stop_Percentage")
		
		def transform_vela_iterator(bars:Int,  last_ema_Fast:Double, last_ema_Slow:Double, last_ema_12:Double, last_ema_26:Double, MACD_implicit:Double, MACD_standard :Double, Signal_standard:Double, Signal_implicit:Double, MACD_standard_9_history:Map[Int, Double], MACD_implicit_9_history:Map[Int, Double], Histogram_MACD_standard	:Double, Histogram_MACD_implicit	:Double, accumulator: Vector[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)]
		                           ):Vector[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)]=
		{

			cabeza_Lectora.vela_Actual_=(bars)
			if (cabeza_Lectora.hasNext)
			{
				val (previous_Candle, actual_Candle, next_Candle)=(cabeza_Lectora.getVela(2),cabeza_Lectora.getVela(1),cabeza_Lectora.getVela(0))
				//assert(next_Candle.openTime>actual_Candle.closeTime && actual_Candle.openTime)
				val minEMA =last_ema_Fast.min(last_ema_Slow)
				val maxEMA = last_ema_Fast.max(last_ema_Slow)
				val actual_open = next_Candle.open //ultima vela que  se ha metido al historial de velas

				val LastClose = previous_Candle.getClose // penultima vela que se metio al historial de velas
				val buyCondition = LastClose < maxEMA && maxEMA < actual_open // cruce de media de la ultima vela
				val sellCondition = LastClose > minEMA && minEMA > actual_open

				val trade_condition=if (next_Candle.openTime> min_Date)
				{
					if (buyCondition) 1
					else if (sellCondition) -1
					else 0
				}else 0

				assert(trade_condition==0||(buyCondition&& next_Candle.open>maxEMA) || sellCondition&next_Candle.open<minEMA,s" there are problems with the creation of the trade,last close: ${actual_Candle.close} open: ${next_Candle.open} min ema= ${minEMA}  max ema= ${maxEMA} is buy $buyCondition")
				val new_ema_Fast = last_ema_Fast + alfa_Fast * (next_Candle.getClose - last_ema_Fast)
				val new_ema_slow =	last_ema_Slow + alfa_Slow * (next_Candle.getClose - last_ema_Slow)
				val new_EMA12 = last_ema_12 + alfa_12 * (next_Candle.getClose - last_ema_12)
				val new_EMA26 = last_ema_26 + alfa_26 * (next_Candle.getClose - last_ema_26)
				val new_MACD_standard = new_EMA12 - new_EMA26
				val new_MACD_implicit = new_ema_Fast - new_ema_slow
				val new_Signal_standard = Signal_standard.toDouble + MACD_standard.toDouble / 9d - MACD_standard_9_history(8) / 9d
				val new_Signal_implicit = Signal_implicit.toDouble + MACD_implicit.toDouble / 9d - MACD_implicit_9_history(8) / 9d
				val new_Histogram_MACD_standard = MACD_standard - Signal_standard
				val new_Histogram_MACD_implicit = MACD_implicit - Signal_implicit
				val new_elem: (Int, Int, Double, Double, Double, Double, Double, Double, Double, Double) =(trade_condition,bars,last_ema_Fast,last_ema_Slow,MACD_standard,MACD_implicit,Signal_standard,Signal_implicit,Histogram_MACD_standard,Histogram_MACD_implicit)
			
				transform_vela_iterator(
					bars+1,
					new_ema_Fast,
					new_ema_slow,
					new_EMA12,
					new_EMA26,
					new_MACD_implicit,
					new_MACD_standard ,
					new_Signal_standard,
					new_Signal_implicit,
					MACD_standard_9_history.map(x=> if(x._1==8)(0,new_Signal_standard) else(x._1+1,x._2) ),
					MACD_implicit_9_history.map(x=> if(x._1==8)(0,new_Signal_implicit) else(x._1+1,x._2) ),
					new_Histogram_MACD_standard,
					new_Histogram_MACD_implicit,
					if(trade_condition!=0 ) accumulator:+new_elem else accumulator)
			}
			else
			{
				cabeza_Lectora.reset
				accumulator
			}
		}
		
		def update_Vela_iterator(bars:Int, last_ema_Fast:Double, last_ema_Slow:Double, last_ema_12:Double, last_ema_26:Double, MACD_implicit:Double, MACD_standard :Double, Signal_standard:Double, Signal_implicit:Double, MACD_standard_9_history:Map[Int, Double], MACD_implicit_9_history:Map[Int, Double], Histogram_MACD_standard	:Double, Histogram_MACD_implicit	:Double, accumulator: Vector[VegasOrder]
		                        ): Vector[VegasOrder]=
		{
			cabeza_Lectora.vela_Actual_=(bars)
			if (cabeza_Lectora.hasNext)
			{
				val (previous_Candle, actual_Candle, next_Candle)=(cabeza_Lectora.getVela(2),cabeza_Lectora.getVela(1),cabeza_Lectora.getVela(0))
				//val actual_Candle=cabeza_Lectora.actual//this is the candle that just closed
				//val next_Candle=cabeza_Lectora.next //this is the candle that will close after this trade

				val startCond = min_Date<actual_Candle.closeTime// bars > Math.max(Math.max(period_EmaFast + 10, period_EmaSlow + 10), 36)  &&
				val new_accumulator: Vector[VegasOrder]=
				if (startCond)// && chCondition ) //no quiero hacer trades antes de 200 bar esperando a que las medias se estabilicen
				{
					val (minEMA, maxEMA) = if(last_ema_Fast<last_ema_Slow) (last_ema_Fast,last_ema_Slow) else (last_ema_Slow,last_ema_Fast)

					//val chCondition = minChannel <= actualCH && actualCH <= maxChannel
					val ActualClose = actual_Candle.getClose //ultima vela que  se ha metido al historial de velas
					val LastClose = previous_Candle.getClose // penultima vela que se metio al historial de velas
					lazy val buyCondition = LastClose < maxEMA && maxEMA < ActualClose // cruce de media de la ultima vela
					lazy val sellCondition = LastClose > minEMA && minEMA > ActualClose

					//	val buyEntryCondition = (minEntry < buyEntry) && (buyEntry < maxEntry)
					//	val sellEntryCondition = (minEntry < sellEntry) && (sellEntry < maxEntry)
					//	val isBuy=buyCondition && buyEntryCondition
					//	val isSell=sellCondition && sellEntryCondition
					
					if (buyCondition)
						Trade(true,   last_ema_Fast.toFloat, last_ema_Slow.toFloat, MACD_standard.toFloat, MACD_implicit.toFloat, Signal_standard.toFloat, Signal_implicit.toFloat, Histogram_MACD_standard.toFloat, Histogram_MACD_implicit.toFloat,stop_Percentage,option_Trail_Stop_1,option_Trail_Stop_2,cabeza_Lectora.getTickReader,"",bars, accumulator) //compra
					else if (sellCondition)
						Trade(false,   last_ema_Fast.toFloat, last_ema_Slow.toFloat, MACD_standard.toFloat, MACD_implicit.toFloat, Signal_standard.toFloat, Signal_implicit.toFloat, Histogram_MACD_standard.toFloat, Histogram_MACD_implicit.toFloat,stop_Percentage,option_Trail_Stop_1,option_Trail_Stop_2,cabeza_Lectora.getTickReader,"",bars, accumulator)
					else accumulator
				} else accumulator
				
				for (trade <- new_accumulator) trade.update(cabeza_Lectora_Details, next_Candle)//actualiza los trades

				val new_ema_Fast = last_ema_Fast + alfa_Fast * (next_Candle.getClose - last_ema_Fast)
				val new_ema_slow =	last_ema_Slow + alfa_Slow * (next_Candle.getClose - last_ema_Slow)
				val new_EMA12 = last_ema_12 + alfa_12 * (next_Candle.getClose - last_ema_12)
				val new_EMA26 = last_ema_26 + alfa_26 * (next_Candle.getClose - last_ema_26)
				val new_MACD_standard = new_EMA12 - new_EMA26
				val new_MACD_implicit = new_ema_Fast - new_ema_slow
				val new_Signal_standard = Signal_standard + MACD_standard / 9d - MACD_standard_9_history(8) / 9d
				val new_Signal_implicit = Signal_implicit + MACD_implicit / 9d - MACD_implicit_9_history(8) / 9d
				val new_Histogram_MACD_standard = MACD_standard - Signal_standard
				val new_Histogram_MACD_implicit = MACD_implicit - Signal_implicit
				update_Vela_iterator(bars+1,
					new_ema_Fast,
					new_ema_slow,
					new_EMA12,
					new_EMA26,
					new_MACD_implicit,
					new_MACD_standard ,
					new_Signal_standard,
					new_Signal_implicit,
					MACD_standard_9_history.map(x=> if(x._1==8)(0,new_Signal_standard) else(x._1+1,x._2) ),
					MACD_implicit_9_history.map(x=> if(x._1==8)(0,new_Signal_implicit) else(x._1+1,x._2) ),
					new_Histogram_MACD_standard,
					new_Histogram_MACD_implicit,
					new_accumulator )
			}
			else 
			{
				cabeza_Lectora.reset
				accumulator.filter(!_.isOpen)
			}
		}
		 
		
		 
		def to_return_2 ={
			cabeza_Lectora.vela_Actual_=(0)
			val trade_signals: Seq[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)] = transform_vela_iterator(
				0,
			cabeza_Lectora.actual.getClose,//last_ema_Fast
			cabeza_Lectora.actual.getClose,//last_ema_Slow
			cabeza_Lectora.actual.getClose,//last_ema_12
			cabeza_Lectora.actual.getClose,//last_ema_26
			0d,//MACD_implicit
			0d,//MACD_standard
			0d,//Signal_standard
			0d,//Signal_implicit
			starter_MACD_standard_9_history,
			starter_MACD_implicit_9_history,
			0,//Histogram_MACD_standard
			0,//Histogram_MACD_implicit
			Vector.empty[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)]//acumulator
			)
			val last_Signal=(trade_signals.last,cabeza_Lectora.length-1)
			 trade_signals
			  .scanRight(last_Signal)((x, y)=>
			  {
				if(x._1==y._1._1)(x,y._2) else (x,y._1._2) // aqui busco fecha de vuelta canela
			})
			  .toVector.distinct//.par
			  .flatMap(x=> {
				 val trade_condition=x._1._1
				 val num_vela_Actual=x._1._2
				 val last_ema_Fast=x._1._3
				 val last_ema_Slow =x._1._4
				 val MACD_standard=x._1._5
				 val MACD_implicit=x._1._6
				 val Signal_standard=x._1._7
				 val Signal_implicit=x._1._8
				 val Histogram_MACD_standard=x._1._9
				 val Histogram_MACD_implicit =x._1._10
				 val last_candle=x._2

				 cabeza_Lectora.vela_Actual_=(num_vela_Actual)

				  val is_buy = trade_condition == 1
				  val tk = cabeza_Lectora.getFirstTick
				  val price = if (is_buy) tk.getAsk else tk.getBid
				 	val (max_ema, min_ema) = if (last_ema_Fast < last_ema_Slow) (last_ema_Slow, last_ema_Fast) else (last_ema_Fast, last_ema_Slow)
				  val entry = if (is_buy) price - max_ema else min_ema - price
				 assert(entry>0 ,s"entry should be a positive number given that is buy: $is_buy price=$price max_ema=$max_ema min_ema=$min_ema")
				  val stop = if (stop_Percentage == 0) 0.0f else if (is_buy) (FastMath.exp(-stop_Percentage / 10000) * price).toFloat else (FastMath.exp(stop_Percentage / 10000) * price).toFloat

				  (option_Trail_Stop_1 to option_Trail_Stop_2).map(ts => (num_vela_Actual, last_candle, new VegasOrder(
					  cabeza_Lectora.paridad,
					  tk,
					  stop,
					  1000,
					  is_buy,
						num_vela_Actual,
						last_ema_Fast.toFloat,
						last_ema_Slow.toFloat,
						MACD_standard.toFloat,
						MACD_implicit.toFloat,
						Signal_standard.toFloat,
						Signal_implicit.toFloat,
						Histogram_MACD_standard.toFloat,
					  Histogram_MACD_implicit.toFloat,
					  entry.toFloat,
					  (max_ema- min_ema).toFloat,
					  ts
				  )))
			  })
			  .map(x=>{
				  x._3.update_to_close(cabeza_Lectora,cabeza_Lectora_Details,x._1,x._2 )
				  x._3}).filter(!_.isOpen)
			  .toVector
			  .groupBy(_.optionTS)
			  .mapValues(x=>  x.sortWith(_.timeRequest<_.timeRequest))
			  .toVector
			  .sortBy(_._1)
		}
		
		def to_return=
		{
			cabeza_Lectora.vela_Actual_=(0)
			update_Vela_iterator(0,//bars
			cabeza_Lectora.actual.getClose,//last_ema_Fast
			cabeza_Lectora.actual.getClose,//last_ema_Slow
			cabeza_Lectora.actual.getClose,//last_ema_12
			cabeza_Lectora.actual.getClose,//last_ema_26
			0d,//MACD_implicit
			0d,//MACD_standard
			0d,//Signal_standard
			0d,//Signal_implicit
			starter_MACD_standard_9_history,
			starter_MACD_implicit_9_history,
			0,//Histogram_MACD_standard
			0,//Histogram_MACD_implicit
			Vector.empty[VegasOrder]//acumulator
		).
			groupBy(_.optionTS)
			.mapValues(x=> x.sortWith(_.timeRequest<_.timeRequest))
			.toVector
			.sortBy(_._1)
			
		}
	/*	println(TimeStamp+s"  testing began  of ema1: $period_EmaFast, ema2: $period_EmaSlow, ts1: $option_Trail_Stop_1,to ts2: $option_Trail_Stop_2 ")
		val t0=Instant.now
		val res=to_return_2
		val t1=Instant.now()
		val diff=java.time.Duration.between(t0,t1)
		println(TimeStamp+s" Ended testing of ema1: $period_EmaFast, ema2: $period_EmaSlow, ts1: $option_Trail_Stop_1,to ts2: $option_Trail_Stop_2 in $diff")
		res*/
		to_return_2
	}

	def velas_to_orders(
											 cabeza_Lectora: CandleLector,
											 cabeza_Lectora_Details: CandleLector,
											 test_point:point,
											 TS_func: Function1[ Double,Int]
										 )
	:Vector[VegasOrder]={

		cabeza_Lectora.reset
		lazy val period_EmaFast:Int=test_point.fast_ema
		lazy val period_EmaSlow:Int=test_point.slow_ema
		lazy val stop_Percentage: Float= test_point.stop.toFloat
		lazy val alfa_Fast:Double=2d/(1d+period_EmaFast.toDouble)
		lazy val alfa_Slow:Double=2d/(1d+period_EmaSlow.toDouble)
		lazy val alfa_12 =2d/(1d+12d)// 0.153846153846153846d
		lazy val alfa_26 =2d/(1d+26d)// 0.074074074074074074d
		lazy val starter_MACD_standard_9_history:Map[Int, Double]=Map((0,0d),(1,0d),(2,0d),(3,0d),(4,0d),(5,0d),(6,0d),(7,0d),(8,0d))
		lazy val starter_MACD_implicit_9_history:Map[Int, Double]=Map((0,0d),(1,0d),(2,0d),(3,0d),(4,0d),(5,0d),(6,0d),(7,0d),(8,0d))
		lazy val min_Date = Stats.year2010


		def transform_vela_iterator(bars:Int,  last_ema_Fast:Double, last_ema_Slow:Double, last_ema_12:Double, last_ema_26:Double, MACD_implicit:Double, MACD_standard :Double, Signal_standard:Double, Signal_implicit:Double, MACD_standard_9_history:Map[Int, Double], MACD_implicit_9_history:Map[Int, Double], Histogram_MACD_standard	:Double, Histogram_MACD_implicit	:Double, accumulator: Vector[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)]
															 ):Vector[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)]=
		{

			cabeza_Lectora.vela_Actual_=(bars)
			if (cabeza_Lectora.hasNext)
			{
				val (previous_Candle, actual_Candle, next_Candle)=(cabeza_Lectora.getVela(2),cabeza_Lectora.getVela(1),cabeza_Lectora.getVela(0))
				val minEMA =last_ema_Fast.min(last_ema_Slow)
				val maxEMA = last_ema_Fast.max(last_ema_Slow)
				val ActualClose = actual_Candle.getClose //ultima vela que  se ha metido al historial de velas

				val LastClose = previous_Candle.getClose // penultima vela que se metio al historial de velas
			val buyCondition = LastClose < maxEMA && maxEMA < ActualClose // cruce de media de la ultima vela
			val sellCondition = LastClose > minEMA && minEMA > ActualClose
				val trade_condition=if (next_Candle.openTime> min_Date)
				{
					if (buyCondition) 1
					else if (sellCondition) -1
					else 0
				}else 0
				val new_ema_Fast = last_ema_Fast + alfa_Fast * (next_Candle.getClose - last_ema_Fast)
				val new_ema_slow =	last_ema_Slow + alfa_Slow * (next_Candle.getClose - last_ema_Slow)
				val new_EMA12 = last_ema_12 + alfa_12 * (next_Candle.getClose - last_ema_12)
				val new_EMA26 = last_ema_26 + alfa_26 * (next_Candle.getClose - last_ema_26)
				val new_MACD_standard = new_EMA12 - new_EMA26
				val new_MACD_implicit = new_ema_Fast - new_ema_slow
				val new_Signal_standard = Signal_standard.toDouble + MACD_standard.toDouble / 9d - MACD_standard_9_history(8) / 9d
				val new_Signal_implicit = Signal_implicit.toDouble + MACD_implicit.toDouble / 9d - MACD_implicit_9_history(8) / 9d
				val new_Histogram_MACD_standard = MACD_standard - Signal_standard
				val new_Histogram_MACD_implicit = MACD_implicit - Signal_implicit
				val new_elem: (Int, Int, Double, Double, Double, Double, Double, Double, Double, Double) =(trade_condition,bars,last_ema_Fast,last_ema_Slow,MACD_standard,MACD_implicit,Signal_standard,Signal_implicit,Histogram_MACD_standard,Histogram_MACD_implicit)

				transform_vela_iterator(
					bars+1,
					new_ema_Fast,
					new_ema_slow,
					new_EMA12,
					new_EMA26,
					new_MACD_implicit,
					new_MACD_standard ,
					new_Signal_standard,
					new_Signal_implicit,
					MACD_standard_9_history.map(x=> if(x._1==8)(0,new_Signal_standard) else(x._1+1,x._2) ),
					MACD_implicit_9_history.map(x=> if(x._1==8)(0,new_Signal_implicit) else(x._1+1,x._2) ),
					new_Histogram_MACD_standard,
					new_Histogram_MACD_implicit,
					if(trade_condition!=0 ) accumulator:+new_elem else accumulator)
			}
			else
			{
				cabeza_Lectora.reset
				accumulator
			}
		}

		def to_return_2={
			cabeza_Lectora.vela_Actual_=(0)
			val trade_signals: Seq[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)] = transform_vela_iterator(
				0,
				cabeza_Lectora.actual.getClose,//last_ema_Fast
				cabeza_Lectora.actual.getClose,//last_ema_Slow
				cabeza_Lectora.actual.getClose,//last_ema_12
				cabeza_Lectora.actual.getClose,//last_ema_26
				0d,//MACD_implicit
				0d,//MACD_standard
				0d,//Signal_standard
				0d,//Signal_implicit
				starter_MACD_standard_9_history,
				starter_MACD_implicit_9_history,
				0,//Histogram_MACD_standard
				0,//Histogram_MACD_implicit
				Vector.empty[(Int, Int, Double, Double, Double, Double, Double, Double, Double, Double)]//acumulator
			)
			val last_Signal=(trade_signals.last,cabeza_Lectora.length-1)
			trade_signals
				.scanRight(last_Signal)((x, y)=>
				{
					if(x._1==y._1._1)(x,y._2) else (x,y._1._2) // aqui busco fecha de vuelta canela
				})
				.toVector.distinct//.par
				.map(x=> {
				val trade_condition=x._1._1
				val num_vela_Actual=x._1._2
				val last_ema_Fast=x._1._3
				val last_ema_Slow =x._1._4
				val MACD_standard=x._1._5
				val MACD_implicit=x._1._6
				val Signal_standard=x._1._7
				val Signal_implicit=x._1._8
				val Histogram_MACD_standard=x._1._9
				val Histogram_MACD_implicit =x._1._10
				val last_candle=x._2

				cabeza_Lectora.vela_Actual_=(num_vela_Actual)

				val is_buy = trade_condition == 1
				val tk = cabeza_Lectora.getFirstTick
				val price = if (is_buy) tk.getAsk else tk.getBid
				val (max_ema, min_ema) = if (last_ema_Fast < last_ema_Slow) (last_ema_Slow, last_ema_Fast) else (last_ema_Fast, last_ema_Slow)
				val entry = if (is_buy) price - max_ema else min_ema - price
				val stop = if (stop_Percentage == 0) 0.0f else if (is_buy) (FastMath.exp(-stop_Percentage / 10000) * price).toFloat else (FastMath.exp(stop_Percentage / 10000) * price).toFloat
				val ts=TS_func(max_ema- min_ema)
				(num_vela_Actual, last_candle, new VegasOrder(
					cabeza_Lectora.paridad,
					tk,
					stop,
					1000,
					is_buy,
					num_vela_Actual,
					last_ema_Fast.toFloat,
					last_ema_Slow.toFloat,
					MACD_standard.toFloat,
					MACD_implicit.toFloat,
					Signal_standard.toFloat,
					Signal_implicit.toFloat,
					Histogram_MACD_standard.toFloat,
					Histogram_MACD_implicit.toFloat,
					entry.toFloat,
					(max_ema- min_ema).toFloat,
					ts
				))
			})
				.map(x=>{
					x._3.update_to_close(cabeza_Lectora,cabeza_Lectora_Details,x._1,x._2 )
					x._3})
				.filter(!_.isOpen)
		  	.sortWith(_.timeRequest<_.timeRequest)


		}


		/*	println(TimeStamp+s"  testing began  of ema1: $period_EmaFast, ema2: $period_EmaSlow, ts1: $option_Trail_Stop_1,to ts2: $option_Trail_Stop_2 ")
      val t0=Instant.now
      val res=to_return_2
      val t1=Instant.now()
      val diff=java.time.Duration.between(t0,t1)
      println(TimeStamp+s" Ended testing of ema1: $period_EmaFast, ema2: $period_EmaSlow, ts1: $option_Trail_Stop_1,to ts2: $option_Trail_Stop_2 in $diff")
      res*/
		to_return_2
	}
	
	
	def print_orders(orders:Vector[VegasOrder], params:point, paridad:String, dir:String):Unit=
	{
		val fileName = paridad +s"""${VegasOptimizer.zonaTS2(params.ts)}\\"""+paridad+s"""${params.fast_ema.toString}-${ params.slow_ema}-ts${params.ts}-stop${params.stop}.csv"""
		val toWrite = new File(dir +  fileName)
		if (!toWrite.exists) {
			if (!toWrite.getParentFile.exists) toWrite.getParentFile.mkdirs
			toWrite.createNewFile
		}
		import java.io.FileWriter
		val writer = new FileWriter(toWrite, false)
		writer.write(orders.head.getHeader + "\r\n")
		def write_results(toWrite:List[VegasOrder]): Unit =
		{
			toWrite match
			{
				case x::tail=>
				{
					writer.write(x.getStringRepresentation+"\r\n")
					write_results(tail)
				}
				case _ => writer.close
				
			}
		}
		write_results(orders.toList)
		
	}


}
