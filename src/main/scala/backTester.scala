package algoTrading.Tester.kernel

import algoTrading.vegas.kernel.point
import algoTrading.velas.kernel.{Tick, _}

import scala.collection.mutable.MutableList

abstract class backTester protected(val cabezaLectora: CandleLector, val cabezaLectoraDetails: CandleLector, val tickByTick: Boolean) extends Runnable {
	cabezaLectora.reset
	protected var alive               :Boolean            = false
	protected var testing             :Boolean            = false
	protected var hasRunned           :Boolean            = false
	protected var setOfOpenOrders     :MutableList[Order] = new MutableList[Order]
	protected var setOfOrders         :MutableList[Order] = new MutableList[Order]
	protected var actualCandle        :Candle             = cabezaLectora.actual //pasado
	protected var lastTick            :Tick               = cabezaLectora.getFirstTick //presente
          	val symbol              :String             = cabezaLectora.getSymbol
       lazy val strategyGlobalStats :Stats              =  Stats(point(10,15,1,100),setOfOrders)
	
	//def updateOrders() = setOfOrders=setOfOrders.filter(_.update(cabezaLectoraDetails, actualCandle))
	
	
	def getStatsResults(globalStats: Stats) = "AcumPyg: " + String.valueOf(globalStats.acumPYG) + "\r\n" + "BestRunUp: " + String.valueOf(globalStats.bestRunUp) + "\r\n" + "WorstDrawDown: " + String.valueOf(globalStats.worstDrawDown) + "\r\n" + "PRR: " + String.valueOf(globalStats.PRR) + "\r\n"
	
	def isAlive = alive
	
	def calculateStats(): Unit
	
	override def run(): Unit
}