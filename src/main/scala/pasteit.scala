package algoTrading.velas.kernel

import java.io.{File, FileReader, RandomAccessFile}
import java.time.format.DateTimeFormatter

import algoTrading.Tester.kernel._

import scala.collection.immutable.LinearSeq

object pasteit {
	
	def main(args: Array[String]):Unit = {
		pasteit.paste(false)
	}
	val millis5Min:Long = 300000l
	val millis1Hour:Long = 3600000l
	val in_Extension = ".csv"
	val out_Extension = ".dat"
	val dir = "C:\\fxdata\\"
	val dirOut = "C:\\fxdataOUT\\"
	
	
	def getLastRecord(toDetect: File):Tick =
	{
		val fileHandler = new RandomAccessFile(toDetect, "rw")
		val fileLength = fileHandler.length
		if (fileLength <= 0) {
			fileHandler.close()
			return new Tick(Stats.year2009, 0.1f, 0.11f)
		}
		else if (fileLength < Tick.byteLength) {
			fileHandler.setLength(0)
			fileHandler.close()
			return new Tick(Stats.year2009, 0.1f, 0.11f)
		}
		else if (fileLength % Tick.byteLength != 0) {
			fileHandler.setLength(fileLength - fileLength % Tick.byteLength)
			fileHandler.close()
			return getLastRecord(toDetect)
		}
		fileHandler.seek(fileLength - Tick.byteLength)
		val buff = new Array[Byte](Tick.byteLength)
		fileHandler.readFully(buff)
		fileHandler.close()
		Tick(buff)
	}

	
	def TimeStamp:String = "[" +DateTimeFormatter.ofPattern("d MMM yyyy HH:mm:ss.SSS").format(java.time.ZonedDateTime.ofInstant(java.time.Instant.now, java.time.ZoneId.systemDefault)) + "]"
	
	def paste(isTrueFX: Boolean):Stream[(String, File)] = {
		def paste_iterator(files_not_procesed:Stream[(String, File)]):Stream[(String, File)]=
		{
			files_not_procesed match
			{
				case Stream.Empty =>
				{
					Stream.Empty 	
				}
				case x#::Stream.Empty =>
				{
					pasteFile(x)
					Stream.Empty
				}
				case x#::tail=>
				{
					pasteFile(x)
					paste_iterator(tail)
				}
			}
		}
		//val isTrueFX=false
		val str0 = if (isTrueFX) ""		else "DAT_ASCII_"
		val str1 = if (isTrueFX) "-"	else "_T_"
		val sep = if (isTrueFX) "-"		else ""
		val Symbol: Stream[(String, File)] =
		Array("AUDCAD", "AUDCHF", "AUDJPY", "AUDNZD", "AUDUSD", "AUXAUD", "BCOUSD", "CADCHF", "CADJPY", "CHFJPY", "ETXEUR", "EURAUD", "EURCAD", "EURCHF", "EURCZK", "EURDKK", "EURGBP", "EURHUF", "EURJPY", "EURNOK", "EURNZD", "EURPLN", "EURSEK", "EURTRY", "EURUSD", "FRXEUR", "GBPAUD", "GBPCAD", "GBPCHF", "GBPJPY", "GBPNZD", "GBPUSD", "GRXEUR", "HKXHKD", "JPXJPY", "NSXUSD", "NZDCAD", "NZDCHF", "NZDJPY", "NZDUSD", "SGDJPY", "SPXUSD", "UDXUSD", "UKXGBP", "USDCAD", "USDCHF", "USDCZK", "USDDKK", "USDHKD", "USDHUF", "USDJPY", "USDMXN", "USDNOK", "USDPLN", "USDSEK", "USDSGD", "USDTRY", "USDZAR", "WTIUSD", "XAGUSD", "XAUAUD", "XAUCHF", "XAUEUR", "XAUGBP", "XAUUSD", "ZARJPY")
				.toStream
				.flatMap(x=>
					(2000 to 2040).
						flatMap(y=>
							(1 to 12).map(z=>if(z<10) y.toString+sep+"0"+z.toString else y.toString+sep+z.toString  ) )
						.map(w=>(x,str0+x+str1+w))
				).map(x=>(x._1,new File(pasteit.dir +x._2+pasteit.in_Extension))).filter(_._2.exists())
		Symbol.foreach(println)
		paste_iterator(Symbol)
		
		
	}

	def ticks_to_candle(ticks:LinearSeq[Tick],millis_length:Long, ticks_parsed:Long):Array[Byte]= {
		val pre_candles: Vector[(Long, Int, (Tick, Tick, Tick, Tick))] = ticks
			.groupBy(x => x.dateTime - (x.dateTime % millis_length))
			.mapValues(x => (x.size, x))
			.toVector
			.par
			.map(x => (x._1, x._2._1, x._2._2
				.map(y => (y, y, y, y))
				.reduce((w, z) => (
					if (w._1.dateTime < z._1.dateTime) w._1 else z._1,
					if (w._2.getMidP < z._2.getMidP) z._2 else w._2,
					if (w._3.getMidP < z._3.getMidP) w._3 else z._3,
					if (w._4.dateTime < z._4.dateTime) z._4 else w._4
				))
			))
			.toVector
			.sortWith(_._1 < _._1)
		val first_Candle = new Candle(
			pre_candles.head._3._1.getMidP,
			pre_candles.head._3._2.getMidP,
			pre_candles.head._3._3.getMidP,
			pre_candles.head._3._4.getMidP,
			pre_candles.head._1,
			pre_candles.head._1 + millis_length,
			pre_candles.head._3._2.dateTime,
			pre_candles.head._3._3.dateTime,
			pre_candles.head._2,
			ticks_parsed
		)
		
		pre_candles.tail.scanLeft(first_Candle)((prev_candle, to_build) => {
			assert(prev_candle.closeTime <= to_build._1, s"building wrong candles not in order \n $prev_candle \n $to_build")
			assert((to_build._1 - prev_candle.openTime) % millis_length == 0, s"building wrong candles open times not multiple of the same length $prev_candle $to_build")
			new Candle(
				to_build._3._1.getMidP,
				to_build._3._2.getMidP,
				to_build._3._3.getMidP,
				to_build._3._4.getMidP,
				to_build._1,
				to_build._1 + millis_length,
				to_build._3._2.dateTime,
				to_build._3._3.dateTime,
				to_build._2,
				prev_candle.ticks.toLong + prev_candle.getTicks_before
			)
		})
			.flatMap(_.getBytes)
			.toArray
	}
	

	
	def pasteFile(params:(String, File) ):Unit = {
		val Symbols:String=params._1
		val toRead:File = params._2
		println(pasteit.TimeStamp + " Begin: " + toRead.getName + "\r")
		
		val toWriteTicks = new File(pasteit.dirOut + Symbols + pasteit.out_Extension)
		val toWrite5min = new File(pasteit.dirOut + Symbols + "_5minutes" + pasteit.out_Extension)
		val toWrite1H = new File(pasteit.dirOut + Symbols + "_1Hour" + pasteit.out_Extension)
		
		val  initialTime:Long= if (!toWriteTicks.exists)
		{
				if(!toWriteTicks.getParentFile.exists)
					toWriteTicks.getParentFile.mkdirs
			toWriteTicks.createNewFile
			Stats.year2009
		}
		else pasteit.getLastRecord(toWriteTicks).dateTime
		
		if (!toWrite5min.exists) toWrite5min.createNewFile
		if (!toWrite1H.exists) toWrite1H.createNewFile
		println(pasteit.TimeStamp + " actual time:" + new java.text.SimpleDateFormat("yyyyMMdd HHmmssSSS").format(new java.sql.Timestamp(initialTime)) + "\r")
		val writer5min = new RandomAccessFile(toWrite5min, "rw")
		val writer1H = new RandomAccessFile(toWrite1H, "rw")
		writer5min.seek(writer5min.length)
		writer1H.seek(writer1H.length)
		val in: FileReader = new FileReader(toRead)
		val tick_writer: RandomAccessFile = new RandomAccessFile(toWriteTicks, "rw")
		val writePointer = tick_writer.length
		
		val residue = writePointer % Tick.byteLength.asInstanceOf[Long]
		if (residue != 0) println(pasteit.TimeStamp + "residue" + String.valueOf(residue))
		val ticks_parsed = writePointer / Tick.byteLength.asInstanceOf[Long]
		tick_writer.seek(writePointer)
		
		println(pasteit.TimeStamp+ " begin parsing file" + "\r")
		val t0=java.time.Instant.now

		val lines=io.Source.fromFile(toRead).getLines().toVector.par
	  	.map(x=>(x,Tick.isWellFormated(x)))
	  	
		val it=lines.filter(_._2)
	  	.map(x=>Tick(x._1))
		.filter(_.dateTime>initialTime)
	  	.toVector.sortWith(_.dateTime<_.dateTime)
	  	.toList
		
		val t1=java.time.Instant.now
		val diff1=java.time.Duration.between(t0,t1)
		println(pasteit.TimeStamp+ " end parsing file in "+diff1.toString+s" with ${lines.size-it.size} bad ticks, now creating candles out of ${it.size} ticks" + "\r")
		if(!it.isEmpty)
		{
			val tick_buffer=it.flatMap(_.getBytes).toArray
			val buffer_5min=ticks_to_candle(it,millis5Min, ticks_parsed)
			val buffer_1hour=ticks_to_candle(it,millis1Hour, ticks_parsed)
			val t2=java.time.Instant.now
			val diff2=java.time.Duration.between(t1,t2)
			println(pasteit.TimeStamp+ "end creating candles "+diff2.toString+" now writing lines ..." + "\r")
			tick_writer.write(tick_buffer,0,tick_buffer.length)
			tick_writer.close
			writer5min.write(buffer_5min,0,buffer_5min.length)
			writer5min.close
			writer1H.write(buffer_1hour,0,buffer_1hour.length)
			writer1H.close
			//toRead.delete
			val t3=java.time.Instant.now
			val diff3=java.time.Duration.between(t2,t3)
			println(pasteit.TimeStamp+ " end writing lines in "+diff3.toString+ "\r")
		}
		
		
		
	}
	
}

