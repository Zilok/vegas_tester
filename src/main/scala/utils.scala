package algoTrading.vegas.kernel

import java.io.FileOutputStream
import java.nio.ByteBuffer
import java.time.{Duration, Instant}

import org.apache.commons.math3.random.MersenneTwister
import org.apache.commons.math3.util.FastMath
import org.apache.poi.ss.usermodel.{CellType, Row, Sheet, Workbook}
import org.apache.poi.xssf.usermodel.{XSSFRow, XSSFWorkbook}
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.xssf.streaming.{SXSSFSheet, SXSSFWorkbook}

import scala.annotation.tailrec
import scala.collection.parallel.immutable.ParVector
import scala.collection.{GenMap, GenSeq}
import scala.reflect.ClassTag


object Point_Stat_utils
{
	def averageVectors(points : GenSeq[L2Clusterablepoint]):L2Clusterablepoint= points.reduce(_+_)/points.length
	def std_Dev_Vectors(points: GenSeq[L2Clusterablepoint]):L2Clusterablepoint=
	{
		val mean=averageVectors(points)
		val variance=averageVectors(points
			.map(x=> (x-mean))
			.map(x=> x.exterior_product(x)))
		new L2Clusterablepoint(variance.data.map(FastMath.sqrt))
	}
	def matrix_Distance[T<:clusterable_point](points:GenSeq[T]):Map[(T,T),Double]=
	{
		def matrix_builder_iterator[T<:clusterable_point](toParse:List[T], acum:List[((T,T),Double)]) :List[((T,T),Double)]=
			toParse match
			{
				case Nil => acum
				case x::tail=> matrix_builder_iterator(tail, acum++
					tail.flatMap(y=>
					{
						val dist= x.distance_to(y)
						List(((x,y),dist),((y,x),dist))
					})
				)
			}
		matrix_builder_iterator(points.toList,List[((T,T),Double)]()).toMap
	}


}

abstract class clusterable_point(val data:Array[Double])
{

	//concrete methods
	def populate_row(row:Row, begin_idx:Int):Row=
	{

		row.createCell(begin_idx,CellType.STRING)
		row.getCell(begin_idx).setCellValue("Point")
		data.indices
			.foreach(idx=>
			{
				val this_idx=begin_idx+1+idx
				row.createCell(this_idx,CellType.NUMERIC)
				row.getCell(this_idx).setCellValue(data(idx))
			})
		row
	}
	protected def renormalize( old_mean:Double, old_deviation:Double, new_mean:Double, new_deviation:Double)(value:Double):Double=  if(old_deviation==0d) new_mean else ((value-old_mean)/old_deviation)*new_deviation+new_mean
	private def comparator_iterator(lhs:List[Double], rhs:List[Double]):Boolean= lhs match
	{
		case Nil=> true
		case x::tail=>
		if (rhs.isEmpty) false
		else
		{
			val y=rhs.head
			if (x< y) true
			else if (x==y) comparator_iterator(tail, rhs.tail)
			else false
		}
	}
	def < (other:clusterable_point):Boolean= comparator_iterator(this.data.toList, other.data.toList)
	def <= (other:clusterable_point):Boolean= comparator_iterator(this.data.toList,other.data.toList)||other.data.toList==this.data.toList
	def > (other:clusterable_point):Boolean= comparator_iterator(other.data.toList,this.data.toList)
	def >= (other:clusterable_point):Boolean= comparator_iterator(other.data.toList,this.data.toList)||other.data.toList==this.data.toList
	def rename_point(new_name:String):named_clusterablepoint= new named_clusterablepoint(this.data,new_name)

	//abstract methods
	def exterior_product(other:clusterable_point):clusterable_point
	def +(other:clusterable_point):clusterable_point
	def -(other:clusterable_point):clusterable_point
	def *(scalar:Double):clusterable_point
	def /(scalar:Double):clusterable_point
	def distance_to(other:clusterable_point):Double
	def renormalize( means_dev:Array[(Double,Double)],new_mean:Double, new_deviation:Double,to_new_mean:Boolean):clusterable_point
	def renormalize(means:clusterable_point, dev:clusterable_point, new_mean:Double, new_deviation:Double, to_new_mean:Boolean):clusterable_point
}

class L2Clusterablepoint(override val data:Array[Double]) extends clusterable_point(data)
{
	override def  exterior_product(other:clusterable_point):L2Clusterablepoint=new  L2Clusterablepoint(this.data.zip(other.data).map(x=>x._1*x._2))

	override def +(other:clusterable_point):L2Clusterablepoint = new L2Clusterablepoint(data.zip(other.data).map(x=> (x._1+x._2)) )
	override def -(other:clusterable_point):L2Clusterablepoint = this + (other*(-1D))
	override def *(scalar:Double):L2Clusterablepoint=new L2Clusterablepoint(data.map(_*scalar))
	override def /(scalar:Double):L2Clusterablepoint=new L2Clusterablepoint(data.map(_/scalar))
	override def distance_to(other:clusterable_point):Double = distance.L2(this.data, other.data)

	override def renormalize( means_dev:Array[(Double,Double)],new_mean:Double, new_deviation:Double,to_new_mean:Boolean):L2Clusterablepoint=
	{
		assert(data.length==means_dev.length, s"the new means don't match in length")
		new L2Clusterablepoint(
			data.zip(means_dev) // (value,(mean,deviation))
				.map(x=>
			{
				val (value,(mean,deviation))=x
				if (to_new_mean)
					renormalize(mean, deviation,new_mean,new_deviation)(value)
				else
					renormalize(new_mean,new_deviation,mean, deviation)(value)
			})
		)
	}
	override def renormalize(means:clusterable_point, std_dev:clusterable_point, new_mean:Double, new_deviation:Double, to_new_mean:Boolean):L2Clusterablepoint=
	{
		renormalize(means.data.zip(std_dev.data),new_mean,new_deviation,to_new_mean)

	}
	/*
	def data_to_str(toParse:List[Double], acum:String):String=
		toParse match
		{
			case Nil=> acum+")"
			case x::tail=> data_to_str(tail, acum+f"${x}%1.2f;")
		}
	override def toString=data_to_str(data.toList, "point(" )*/
	def data_to_str(toParse:List[Double], acum:String):String=
	{
		val formatter=java.text.NumberFormat.getInstance(java.util.Locale.ENGLISH)

		formatter.setGroupingUsed(false)
		formatter.setMinimumFractionDigits(0)
		formatter.setMinimumIntegerDigits(1)
		formatter.setMaximumFractionDigits(10)
		formatter.setMaximumIntegerDigits(20)
		toParse match
		{
			case Nil=> acum
			case x::tail=> data_to_str(tail, acum+";"+formatter.format(x))
		}
	}
	override def toString: String =data_to_str(data.toList, "point" )

}

case class named_clusterablepoint(override val data:Array[Double], name:String) extends L2Clusterablepoint(data)
{
	override def populate_row(row:Row, begin_idx:Int):Row=
	{
		val to_Return=super.populate_row(row,begin_idx)
		to_Return.getCell(begin_idx).setCellValue(name)
		to_Return
	}
	override def renormalize(means:clusterable_point, std_dev:clusterable_point, new_mean:Double, new_deviation:Double, to_new_mean:Boolean):named_clusterablepoint=
	{
		val l2Tmp=renormalize(means.data.zip(std_dev.data),new_mean,new_deviation,to_new_mean)
		new named_clusterablepoint(l2Tmp.data,name )
	}
	override def renormalize( means_dev:Array[(Double,Double)],new_mean:Double, new_deviation:Double,to_new_mean:Boolean):L2Clusterablepoint=
	{
		assert(data.length==means_dev.length, s"the new means don't match in length")
		new named_clusterablepoint(
			data.zip(means_dev) // (value,(mean,deviation))
				.map(x=>
			{
				val (value,(mean,deviation))=x
				if (to_new_mean)
					renormalize(mean, deviation,new_mean,new_deviation)(value)
				else
					renormalize(new_mean,new_deviation,mean, deviation)(value)
			}),name
		)
	}
	override def toString: String =data_to_str(data.toList, name )
	def to_bytes:Array[Byte]=
	{
    val name_bytes=name.getBytes("Cp1252")
		val double_byte_size=java.lang.Double.BYTES
		val data_bytes=ByteBuffer.allocate(2+double_byte_size*data.length)

		(0 until data.length).foreach(idx=> data_bytes.putDouble(2+idx*double_byte_size, data(idx)))
    name_bytes++data_bytes.array()
	}
}

object distance
{
	def L1(x1 :Array[Double],x2:Array[Double]):Double=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.sum
	}
	def L2(x1 :Array[Double],x2:Array[Double]):Double=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		FastMath.sqrt(
			x1
				.zip(x2)
				.map(pair=>(pair._1-pair._2)*(pair._1-pair._2))
				.sum
		)
	}
	def LInf(x1 :Array[Double],x2:Array[Double]):Double=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.max
	}

	def L1(x1 :Array[Long],x2:Array[Long]):Long=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.sum
	}
	def L2(x1 :Array[Long],x2:Array[Long]):Double=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		FastMath.sqrt(
			x1
				.zip(x2)
				.map(pair=>(pair._1-pair._2)*(pair._1-pair._2))
				.sum
		)
	}
	def LInf(x1 :Array[Long],x2:Array[Long]):Long=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.max
	}

	def L1(x1 :Array[Float],x2:Array[Float]):Float=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.sum
	}
	def L2(x1 :Array[Float],x2:Array[Float]):Double=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		FastMath.sqrt(
			x1
				.zip(x2)
				.map(pair=>(pair._1-pair._2)*(pair._1-pair._2))
				.sum
		)
	}
	def LInf(x1 :Array[Float],x2:Array[Float]):Float=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.max
	}
	def L1(x1 :Array[Int],x2:Array[Int]):Int=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.sum
	}
	def L2(x1 :Array[Int],x2:Array[Int]):Double=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		FastMath.sqrt(
			x1
				.zip(x2)
				.map(pair=>(pair._1-pair._2)*(pair._1-pair._2))
				.sum
		)
	}
	def LInf(x1 :Array[Int],x2:Array[Int]):Int=
	{
		//assert(x1.length==x2.length, s" vectors don't match ${x1.length}, ${x2.length}")
		x1
			.zip(x2)
			.map(pair=>(pair._1-pair._2).abs)
			.max
	}

}

//noinspection ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol,ScalaUnusedSymbol
// recomended method to use is getClusters(vectors, kmeansKernels):List[(CENTERS ,MIN DISTANCE TO CENTER,MAX DISTANCE TO CENTER,AVG DISTANCE TO CENTER,STD. DEVIATION DISTANCE TO CENTER)]
object knn_classifier
{
	/** K-means parameter: Convergence criteria */
	def kmeansEta: Double = 0.0000000001D

	/** K-means parameter: Maximum iterations */
	def kmeansMaxIterations = 120

	/** Sample the vectors */
	def sampleVectors[T<:clusterable_point](vectors:   GenSeq[T], kmeansKernels:Int)(implicit m: ClassTag[T]): Array[T] = {

		//assert(kmeansKernels % langs.length == 0, "kmeansKernels should be a multiple of the number of languages studied.")
		def reservoirSampling[T<:clusterable_point ](points: GenSeq[T], size: Int)(implicit m: ClassTag[T]): Array[T] =
		{
			val iter=points.toList.iterator
			assert(points.length>size, s"iterator must have at least $size elements")
			val res: Array[T]= points.take(size).toArray
			assert(res.length==size, s"we should take $size vectors, actually got ${res.length}")
			val rnd = new MersenneTwister()
			var i = size.toLong
			while (iter.hasNext)
			{
				val elt = iter.next
				val j = math.abs(rnd.nextInt) % i
				if (j < size)
				{
					res(j.toInt) = elt
				}
				i += 1
			}
			res
		}
		def max_distance_sampler[T<:clusterable_point](vectors:   GenSeq[T], kmeansKernels:Int)(implicit m: ClassTag[T]): Array[T] =
		{
			val map_distance=Point_Stat_utils.matrix_Distance(vectors).toVector.par
			def sampler_iterator[T<:clusterable_point](
																			 candidates: ParVector[((T,T),Double)],
																			 counter:Int,
																			 means:Set[T]):Set[T]=
			{

				if (counter < kmeansKernels)
				{
					//here we assume that if x,y \in means then ((x,y),dist)\notIn candidates and ((y,x),dist)\notIn candidates
					val (pair,dist)= candidates.reduce((P1,P2)=>if (P1._2>P2._2) P1 else P2)
					val (c1,c2)=pair
					val testigoA=means.contains(c1)
					val testigoB=means.contains(c2)
					if (testigoA && testigoB) sampler_iterator(candidates.filterNot(P=>
					{
						val ((x,y),d):((clusterable_point,clusterable_point),Double)=P
						((c1==x)&&(c2==y))||((c1==x)&&(c2==y))
					}
					),counter,means)
					else if(!testigoA&& testigoB) sampler_iterator(candidates,counter+1, means+c1)
					else if(testigoA&& !testigoB) sampler_iterator(candidates,counter+1, means+c2)
					else
					{
						if (counter+1<kmeansKernels) sampler_iterator(candidates,counter+2, means+c1+c2)
						else 					     sampler_iterator(candidates,counter+1, means+c2)
					}

				}
				else means
			}
			val res=sampler_iterator(map_distance,0,Set[T]()).toArray
			assert(kmeansKernels==res.length,s"bad implementation error kernels = $kmeansKernels  and length is ${res.length}")
			res
		}

		val res: Array[T]= reservoirSampling( vectors, kmeansKernels)

		assert(res.length == kmeansKernels, res.length)
		res
	}


	//
	//
	//  Kmeans method:
	//
	//

	/** Main kmeans computation */
	@tailrec final def kmeans[T<:clusterable_point](means:  Array[clusterable_point], vectors:   GenSeq[T], kmeansKernels:Int, iter: Int = 1, debug: Boolean = false): Array[clusterable_point] =
	{
		val t0=Instant.now
		assert(means.length== kmeansKernels, s" means $means.length not equal to kernels  $kmeansKernels")

		val data_with_mean=vectors.map(p=>(p,(findClosest(p,means))))// we have a pair, (point, mean_index)

		val clusters: Map[Int, clusterable_point] =means
			.zipWithIndex
			.map(x=>
			{
				val (center, index)=x
				(
					index,
					data_with_mean
						.filter(index==_._2 )//points that match the given index
						.map(_._1)// the points of the given index
				) // we have a pair (index, list of points near to that mean)
			})
			.filter(_._2.nonEmpty)
			.toMap// this is now a map (index, points that are near to that center)
			.mapValues(averageVectors)// values map to the the new means

		assert(clusters.size <= kmeansKernels, s" clusters ${clusters.size} greater than kernels  $kmeansKernels")
		val newMeans = means.zipWithIndex.map(x=>
			if (clusters.isDefinedAt(x._2)) clusters(x._2)
			else if (debug)
			{
				println(s"cluster is not defined at ${x._2} keeping mean ${x._1}");
				x._1
			} else x._1
		)

		val distance = euclideanDistance(means, newMeans) // the distance between the previous centers and the new ones
		if (debug) {
			val t1=Instant.now
			val dif=Duration.between(t0,t1)
			println(s"""Iteration: $iter
								 								 |  * current distance: $distance
								 								 |  * desired distance: $kmeansEta
								 								 |  * time: $dif""".stripMargin)
			for (idx <- 0 until kmeansKernels)
				println(f"   ${means(idx).toString}%20s ==> ${newMeans(idx).toString}%20s  " +
					f"  distance: ${euclideanDistance(means(idx), newMeans(idx))}%8.2f")
		}

		if (converged(distance))
			newMeans
		else if (iter < kmeansMaxIterations)
			kmeans(newMeans, vectors,kmeansKernels, iter + 1, debug)
		else {
			if (debug)
			{
				println(s"Reached max iterations!: $kmeansMaxIterations And distance is: $distance with expected convergence of $kmeansEta ")
			}
			newMeans
		}
	}

	//
	//
	//  Kmeans utilities:
	//
	//

	/** Decide whether the kmeans clustering converged */
	def converged(distance: Double): Boolean =
		distance < kmeansEta


	/** Return the euclidean distance between two points */
	def euclideanDistance(v1: clusterable_point, v2: clusterable_point): Double =    v1.distance_to(v2)

	/** Return the euclidean distance between two sets of points */
	def euclideanDistance(a1:   GenSeq[clusterable_point], a2:   GenSeq[clusterable_point]): Double =
	{
		assert(a1.length == a2.length)
		var sum = 0d
		var idx = 0
		while(idx < a1.length)
		{
			sum += euclideanDistance(a1(idx), a2(idx))
			idx += 1
		}
		sum
	}

	def Prob_xi_in_GK(xi:clusterable_point, Gk:clusterable_point, mean_K:Double):Double=
	{

		val K:Double=10e13// k/10^Log_10(K) similar to 1
	val T:Double=0.5 // Prob_xi_in_GK = T if distance (xi to Gk) is mean_K
	val z= xi.distance_to(Gk)
		if (mean_K>0)
			K/(K+FastMath.pow(K/T-K,z/(2*mean_K)))
		else z
	}

	/** Return the closest point */
	def findClosest(p: clusterable_point, centers:   GenSeq[clusterable_point]): Int =
		centers
			.zipWithIndex
			.map{case (center_i,i)=>(euclideanDistance(p,center_i),i)}
  		.minBy(_._1)
  		._2


	/** Average the vectors */
	def averageVectors(ps:  GenSeq[clusterable_point]): clusterable_point =
	{
		val len=ps.size
		ps.reduce((vec1,vec2)=> vec1+vec2	)/len
	}




	def isClusterable(vectors:  GenSeq[clusterable_point]):Boolean
	= vectors.map(_.data.length).reduce((x,y)=> if(x==y)x else ((-10).min(x-1)).min(y-1) )>1// this ensures that the vectors are at least two dimentional
def sorting_means[T<:clusterable_point](A:(clusterable_point,(Int,Double,Double,Double,Double),GenSeq[T]), B:(clusterable_point,(Int,Double,Double,Double,Double),GenSeq[T])):Boolean=
{
	def array_comparator(array_A:List[Double],array_B:List[Double]):Boolean=array_A match
	{
		case Nil => 0 > array_B.length
		case a_head::a_tail=>
		array_B match
		{
			case Nil => true
			case b_head::b_tail=>
			if( b_head==a_head)
					array_comparator(a_tail,b_tail)
				else
					a_head>b_head
		}
	}
	if (A._2._1==B._2._1)
		array_comparator(A._1.data.toList,B._1.data.toList)
	else A._2._1<B._2._1
}
	def getClusters[T<:clusterable_point](vectors:  GenSeq[T], kmeansKernels:Int ):List[(clusterable_point,(Int,Double,Double,Double,Double),GenSeq[T])]=
	{
		assert(isClusterable(vectors), "there is some vector with mismatching length")
		val centers: Array[clusterable_point] = kmeans(sampleVectors(vectors,kmeansKernels), vectors, kmeansKernels,0,debug = false)
		var actual_results: GenSeq[(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T])] =getClusters_from_means(vectors,kmeansKernels,centers)
		var old_results=actual_results
		var test:Double=7d
		var counter:Int=0
		while(test>1 && counter<200 )
		{
			counter+=1
			old_results=actual_results
			//val intermediate_results=if(actual_results.length<60) add_new_clusters(actual_results,vectors) else actual_results
			actual_results=fine_tune_clusters(old_results,vectors)
			test=
				if(actual_results.length!=old_results.length) 7d
				else knn_classifier.euclideanDistance(actual_results.map(_._1),old_results.map(_._1))
		}
		actual_results.toList
	}
	private def getClusters_from_means[T<:clusterable_point](vectors:  GenSeq[T], kmeansKernels:Int, centers: Array[clusterable_point] ):List[(clusterable_point,(Int,Double,Double,Double,Double),GenSeq[T])]=
	{
		val data_with_mean=vectors.map(p=>(p,findClosest(p,centers)))// we have a pair, (point, mean_index)
	val stats_on_clusters:Array[(clusterable_point, (Int, Double, Double, Double, Double), List[T])]=centers
		.zipWithIndex
		.map(x=>
		{
			val (center, index)=x
			val points_in_cluster=data_with_mean
				.filter(index==_._2 )//points that match the given index
				.toList.map(_._1)// the points of the given index
		val num_points_in_cluster=points_in_cluster.length
			if (num_points_in_cluster==0)(center,(0,0D,0D,0D,0D),points_in_cluster)
			else
			{
				val dist=points_in_cluster. map( euclideanDistance(center,_))

				val max_distance=dist.max
				val min_distance=dist.min
				val avg_distance=dist.sum/num_points_in_cluster
				val std_dev_distance=FastMath.sqrt(dist.map((_,avg_distance)).map(x=>(x._1-x._2)*(x._1-x._2)).sum/(num_points_in_cluster))
				(
					center,
					(num_points_in_cluster,
						min_distance,
						max_distance,
						avg_distance,
						std_dev_distance),
					points_in_cluster
				) // we have a pair (index, list of points near to that mean)
			}
		})

		stats_on_clusters.filter(_._2._1>0).toList
	}
	def add_new_clusters[T<:clusterable_point]
	(stats_on_clusters:GenSeq[(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T])],
	 vectors:   GenSeq[T],
	 debug: Boolean = false)
	:GenSeq[(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T])]=
	{
		val isolated_points: Array[clusterable_point] =stats_on_clusters.flatMap(x=>
		{
			val (center,(num_points_in_cluster,min_distance,max_distance,avg_distance,std_dev_distance),points_in_cluster)=x
			points_in_cluster.map(actual_point=>(actual_point,
				Prob_xi_in_GK(actual_point,center,avg_distance ))
			).filter(_._2<.5d)
				.map(_._1)
		}).toVector.toArray
		if(isolated_points.length>0)
		{
			val new_clusters=isolated_points++stats_on_clusters.map(_._1)
			val new_clusters2:Array[clusterable_point]=kmeans(new_clusters, vectors, new_clusters.length,0,debug = false)
			getClusters_from_means(vectors,new_clusters2.length,new_clusters2)
		}
		else
			stats_on_clusters
	}
	def fine_tune_clusters[T<:clusterable_point](stats_on_clusters:GenSeq[(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T])], vectors:GenSeq[T], debug: Boolean = false)
	:GenSeq[(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T])]	={
		def merge_clusters(clust1:(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T]), clust2:(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T]))
		:(clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T])
		=
		{
			val new_cluster:GenSeq[T]=clust1._3++clust2._3
			val new_mean=averageVectors(new_cluster)
			val num_points_in_cluster=new_cluster.length
			val dist=new_cluster. map( euclideanDistance(new_mean,_))
			val max_distance=dist.max
			val min_distance=dist.min
			val avg_distance=dist.sum/num_points_in_cluster
			val std_dev_distance=FastMath.sqrt(dist.map((_,avg_distance)).map(x=>(x._1-x._2)*(x._1-x._2)).sum/(num_points_in_cluster))
			(
				new_mean,
				(num_points_in_cluster,
					min_distance,
					max_distance,
					avg_distance,
					std_dev_distance),
				new_cluster
			)
		}

		val cluster_map:GenMap[clusterable_point,(clusterable_point,(Int, Double, Double, Double, Double), GenSeq[T])]= stats_on_clusters.toVector.par.map{ case (x,y,z)=> (x,(x,y,z))}.toMap
		val closest_means=Point_Stat_utils.matrix_Distance(cluster_map.keys.toSeq).toVector.sortWith(_._2<_._2).head
		val cluster1=cluster_map(closest_means._1._1)
		val cluster2=cluster_map(closest_means._1._2)
		val merged: (clusterable_point, (Int, Double, Double, Double, Double), GenSeq[T]) = merge_clusters(cluster1,cluster2)

		if(merged._2._4<1.5)// if the average distance is less than 0.5
		{
			val new_clusters: Array[clusterable_point] =(merged+:	stats_on_clusters)
				.filterNot{ case (x,y,z)=> x==cluster1._1 || x==cluster2._1}
				.map(_._1).toArray
			getClusters_from_means(vectors,new_clusters.length,kmeans(new_clusters, vectors, new_clusters.length,0,debug))
		}
		else stats_on_clusters.toList
	}


	def clusters_to_string(vectors:  GenSeq[named_clusterablepoint], centers: Array[clusterable_point], means:clusterable_point, devs:clusterable_point):GenSeq[String]=vectors
		.map(vec=>
		{
			val tmp_vec=vec.renormalize(means,devs,0,1,to_new_mean = true)
			val index=findClosest(tmp_vec, centers)
			(vec.toString+f";Grupo $index%03d; ${tmp_vec.distance_to(centers(index))}")
		})
	def results_printer(means_results:List[(clusterable_point,(Int,Double,Double,Double,Double),GenSeq[clusterable_point])],
											data_base:GenSeq[named_clusterablepoint],
											means:clusterable_point,
											devs:clusterable_point,
											sheet1:Sheet,
											filename_to_print:String,
											clusterable_points_head:Array[String] ): Unit =
	{

		val toWrite = new java.io.File(filename_to_print)
		if (!toWrite.exists) {
			if (!toWrite.getParentFile.exists) toWrite.getParentFile.mkdirs
			toWrite.createNewFile
		}
		val sheet_to_write=populate_sheet(means_results,
			data_base,
			means,
			devs,
			sheet1,
			clusterable_points_head )
		sheet_to_write.getWorkbook.write( new FileOutputStream(toWrite))
		sheet_to_write.getWorkbook.close
	}
	def array_to_row(to_write:Array[String], row: Row, begin_idx:Int=0):Unit
	=
	{
		val end_idx=begin_idx+to_write.length
		to_write.indices.
			foreach(idx=>
			{
				val cell=row.createCell(begin_idx+idx)
				cell.setCellValue(to_write(idx))
			})
	}
	def concat_headers(to_paste:Array[String],separator:String):String=
	{
		to_paste.toVector.par.reduce((A,B)=> A+separator+B)
	}
	def get_subArray[T](data_in:Array[T], indexes :Array[Int])(implicit m: ClassTag[T]):Array[T]=
	{
		assert(0<=indexes.min && indexes.max<data_in.length)
		//val data_map=data_in.zipWithIndex.groupBy(_._2).mapValues(_._1)
		val to_return=indexes.map(x=>
		{
			val ret=data_in(x)
			//println(s"for index $x we have $ret")
			ret
		})
		assert(to_return.length==indexes.length)
		to_return
	}
	def populate_sheet
	(means_results:List[(clusterable_point,(Int,Double,Double,Double,Double),GenSeq[clusterable_point])],
	 data_base:GenSeq[named_clusterablepoint],
	 means:clusterable_point,
	 devs:clusterable_point,
	 sheet1:Sheet,
	 clusterable_points_head:Array[String] ):Sheet
	=
	{
		val num_grupos=means_results.length
		val clusterable_points_head_array_max:Array[String]=clusterable_points_head.map(z=>z+"_MAX")
		val clusterable_points_head_array_min:Array[String]=clusterable_points_head.map(z=>z+"_MIN")
		val clusterable_points_head_array_avg:Array[String]=clusterable_points_head.map(z=>z+"_AVG")
		val stats_head_array=Array("""Número de grupo""","""Número de elementos""","""Distancia mínima""","""Distancia máxima""","""Distancia promedio""","""Desviación estandar""")
		val complete_stat_head_array:Array[String]=stats_head_array++Array("")++clusterable_points_head++clusterable_points_head_array_avg++clusterable_points_head_array_min++clusterable_points_head_array_max


		val orderd_data_base=data_base.toVector.sortWith((P1,P2)=>
		{
			if(P1.name==P2.name)
				P1 > P2
			else if(P1.name.length==P2.name.length)
				P1.name<P2.name
			else P1.name.length<P2.name.length
		}

		)
		val mean_stats=means_results
			.map(x=>(x,orderd_data_base.map(y=> {
				val norm_y=y.renormalize(means,devs,0,1,to_new_mean = true)
				x._1.distance_to(norm_y)
			}
			).sum))
			.sortWith(_._2>_._2)// here the objective is to set all the far away clusterable_points in farther clusters.
			.map(_._1)
			.zipWithIndex
			.map(x=>
			{

				val ((center, (elements, min_distance, max_distance, avg_distance, dev_distance), cluster_clusterable_points),index)
				:((clusterable_point,(Int,Double,Double,Double,Double),GenSeq[clusterable_point]),Int)=x

				lazy val max_data:Array[Double]=
					cluster_clusterable_points.map(_.renormalize(means,devs,0,1,to_new_mean = false).data).
						reduce((A,B)=> A.zip(B).map(y=> y._1.max(y._2))  )

				lazy val min_data:Array[Double]=	cluster_clusterable_points.map(_.renormalize(means,devs,0,1,to_new_mean = false).data).
					reduce((A,B)=> A.zip(B).map(y=> y._1.min(y._2))  )
				lazy val default_data=(0 to center.data.length).map(x=>0d).toArray
				val max_clusterable_point= named_clusterablepoint(if(cluster_clusterable_points.nonEmpty) max_data else default_data,	"maximo")
				val min_clusterable_point=named_clusterablepoint( if(cluster_clusterable_points.nonEmpty) min_data else default_data,"minimo")
				val avg_clusterable_point=center.renormalize(means,devs,0,1,to_new_mean = false).rename_point("promedio")

				(index,elements, min_distance, max_distance, avg_distance, dev_distance, center.rename_point(f"Grupo $index%03d"),avg_clusterable_point,min_clusterable_point,max_clusterable_point)
			})
		array_to_row(clusterable_points_head,sheet1.createRow(0),0)
		means.rename_point("Mean").populate_row(sheet1.createRow(1),0)
		devs.rename_point("Devs").populate_row(sheet1.createRow(2),0)
		array_to_row(complete_stat_head_array,sheet1.createRow(3),0)
		val clusterable_point_length=1+means.data.length
		//write centers(done)
		(0 until num_grupos).foreach(idx=>
		{
			val row=sheet1.createRow(4+idx)
			val (num_grupo,elements, min_distance, max_distance, avg_distance, dev_distance, center,avg_clusterable_point,min_clusterable_point,max_clusterable_point)=mean_stats(idx)
			row.createCell(0).setCellValue(num_grupo)
			row.createCell(1).setCellValue(elements)
			row.createCell(2).setCellValue(min_distance)
			row.createCell(3).setCellValue(max_distance)
			row.createCell(4).setCellValue(avg_distance)
			row.createCell(5).setCellValue(dev_distance)
			center   .populate_row(row,7+0*clusterable_point_length)
			avg_clusterable_point.populate_row(row,7+1*clusterable_point_length)
			min_clusterable_point.populate_row(row,7+2*clusterable_point_length)
			max_clusterable_point.populate_row(row,7+3*clusterable_point_length)

		})
		//write rest of data (done)
		val heads_data_row=sheet1.createRow(num_grupos+4)
		array_to_row(clusterable_points_head,heads_data_row,0)
		heads_data_row.createCell(clusterable_point_length).setCellValue("Grupo")
		//heads_data_row.createCell(clusterable_point_length+1).setCellValue("Fecha")
		//(0 until mean_stats.length).foreach(kdx=> heads_data_row.createCell(clusterable_point_length+2+kdx).setCellValue(mean_stats(kdx)._7.name))
		(0 until orderd_data_base.length).foreach(idx=>
		{
			val row=sheet1.createRow(5+num_grupos+idx)
			val actual_clusterable_point=orderd_data_base(idx)
			actual_clusterable_point.populate_row(row,0)
			val actual_normal_clusterable_point=actual_clusterable_point.renormalize(means,devs,0,1,to_new_mean = true)
			val distance_to_centers=mean_stats.zipWithIndex.map(x=> {
				val ((num_grupo, elements, min_distance, max_distance, avg_distance, dev_distance, center, avg_clusterable_point, min_clusterable_point, max_clusterable_point),jdx) = x
				val dist=center.distance_to(actual_normal_clusterable_point)
				//row.createCell(center.name.takeRight(3).toInt +clusterable_point_length+2).setCellValue(dist)
				(center,dist)
			})
			val grupo=distance_to_centers.reduce((A,B)=> if(A._2<B._2)A else B)._1.name
			row.createCell(clusterable_point_length).setCellValue(grupo)

			//row.createCell(clusterable_point_length+1).setCellValue(date)

		})
		sheet1
	}
}

object simple_speedy_net
{
	val SIGMOID:Int=1
	val SOFTSIGN:Int=2
	val RELU:Int=3
	val IDENTITY:Int=4
}
class simple_speedy_net(W:Array[Float], b:Array[Float], hidden_dims:Array[Int],activations:Array[Int], num_layers:Int) {
	import java.io.{File,  RandomAccessFile}
	import org.apache.commons.math3.util.FastMath
	private val SIGMOID: Int = simple_speedy_net.SIGMOID
	private val SOFTSIGN: Int = simple_speedy_net.SOFTSIGN
	private val RELU: Int = simple_speedy_net.RELU
	private val IDENTITY: Int = simple_speedy_net.IDENTITY
	private val cache_size = hidden_dims.sum
	private val layer_cache_index: Array[Int] = (0 to num_layers).map(layer => if (layer == 0) 0 else hidden_dims.take(layer).sum).toArray
	private val activation_index: Array[Int] = (0 until num_layers).map(idx => hidden_dims(idx) * hidden_dims(idx + 1)).scanLeft(0)(_ + _).toArray
	private val num_activations = activation_index.last
	private val intermediate_cache: Array[Float] = new Array[Float](cache_size)
	assert(num_layers + 1 == hidden_dims.length, s" hidden layers:${num_layers + 1} doesn't match the expected value ${hidden_dims.length}")
	assert(b.size == cache_size - hidden_dims(0), s"expected cache size ${cache_size - hidden_dims(0)} and given size is ${b.size}")
	assert(num_layers == activations.length, s" hidden layers doesn't match the expected value")
	assert(num_activations == W.size)

	private def g(x: Float, activation: Int): Float = activation match {
		case SIGMOID => g_SIGMOID(x)
		case SOFTSIGN => g_SOFTSIGN(x)
		case RELU => g_RELU(x)
		case IDENTITY => g_IDENTITY(x)
		case _ => x
	}

	private def g_SIGMOID(d: Float): Float = 1 / (1 + FastMath.exp(-d).toFloat)

	private def g_SOFTSIGN(d: Float): Float = d / (1 + d.abs)

	private def g_RELU(d: Float): Float = d.max(0)

	private def g_IDENTITY(d: Float): Float = d

	def feedForward(x: Array[Float]): Array[Float] = {
		assert(x.size == hidden_dims(0))
		System.arraycopy(x, 0, intermediate_cache, 0, x.length)
		for (layer <- 1 to num_layers) //layer 0 is input layer
		{
			val input_Dim = hidden_dims(layer - 1)
			val output_Dim = hidden_dims(layer)

			for (jdx <- 0 until output_Dim) {
				val kdx: Int = jdx + layer_cache_index(layer) //index of actual result should be given by the number of the layer
				intermediate_cache(kdx) = b(kdx - hidden_dims(0))
				//   println(intermediate_cache.toList)
				for (idx <- 0 until input_Dim) {
					val matrix_index: Int = jdx * input_Dim + activation_index(layer - 1) + idx
					val input_index: Int = idx + layer_cache_index(layer - 1)
					intermediate_cache(kdx) = intermediate_cache(kdx) + W(matrix_index) * intermediate_cache(input_index)
					// println(intermediate_cache.toList)
				}
				intermediate_cache(kdx) = g(intermediate_cache(kdx), activations(layer - 1))
				// println(intermediate_cache.toList)
			}
		}
		intermediate_cache.takeRight(hidden_dims.last)
	}
	def toBytes():Array[Byte]=
	{
		val arr_size=(3+activations.length+hidden_dims.length)*java.lang.Integer.BYTES+		(b.length+W.length)*java.lang.Float.BYTES
		println(arr_size)

		import java.nio._
		val buff=ByteBuffer

			.allocate(arr_size)
			.order(ByteOrder.LITTLE_ENDIAN)
			//.order(ByteOrder.BIG_ENDIAN)
			.putInt(0,num_layers)
			.putInt(java.lang.Integer.BYTES,num_activations)
			.putInt(2*java.lang.Integer.BYTES,cache_size - hidden_dims(0))
		var base_index=3*java.lang.Integer.BYTES
		for (actv<-activations)
		{
			buff.putInt(base_index,actv)
			base_index=base_index+java.lang.Integer.BYTES
		}
		for (dim<-hidden_dims)
		{
			buff.putInt(base_index,dim)
			base_index=base_index+java.lang.Integer.BYTES
		}
		for (b_i<-b)
		{
			buff.putFloat(base_index,b_i)
			base_index=base_index+java.lang.Float.BYTES
		}
		for (w_i<-W)
		{
			buff.putFloat(base_index,w_i)
			base_index=base_index+java.lang.Float.BYTES
		}
		buff.array
	}
	def export_to_file(file_to_write:String): Unit =
	{
		val tick_writer: RandomAccessFile = new RandomAccessFile(file_to_write, "rw")
		tick_writer.write(toBytes)
		tick_writer.close()
	}

}


